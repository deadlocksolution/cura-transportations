@extends('layouts.admin_template')
@section('title', 'Términos y Condiciones')
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Términos y Condiciones</h5>
        </div>
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                   
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="text-editor" style="padding: 15px 1%;">
                                                <textarea name="TermsAndCondition" id="TermsAndCondition">{{ $terms}}</textarea>
                                                <script>
                                                        CKEDITOR.replace( 'TermsAndCondition' );
                                                </script>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="form-actions mt-10">
                                        <button type="button" class="btn btn-success mr-10 save-terms"> Save</button>
                                    </div>
                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $('.save-terms').click(function(){
            var TermsAndCondition = CKEDITOR.instances['TermsAndCondition'].getData();

            $.ajax({
                type: "POST",
                url: 'terms/store',
                data: {
                    'TermsAndCondition' : TermsAndCondition,
                },
                success: function (res) {
                    if(res['success']==1){
                        swal('CURA','Términos y condiciones actualizados con éxito','success');
                    }
                }
            })
        })
    </script>
@endpush