@extends('layouts.admin_template')
@section('title', 'Edit Vehicle') 
@section('main_container')

<style type="text/css">
#exist_image_tier{
    margin:10px;
}
#exist_image_tier .img{
    height: 250px;
    width: 100%;
    background-size:contain !important;
    background-repeat: no-repeat !important;
    box-shadow: 1px 1px 5px 1px;
}
#exist_image_tier .delete_product_image_btn{
    float: left;
    position: absolute;
    top: -10px;
    right: 0;
    background:#fff;
}

</style>
<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Edit Vehicle</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('vehicle') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('vehicle.update'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}
                                        <div class="form-body">
                                            <input type="hidden" name="Id" value="{{ $VehicleData->VehicleId }}" id="Id">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="VehicleType">VEHICLE TYPE NAME </label>
                                                        <input type="text" id="VehicleType" pattern=".*\S+.*" name="VehicleType" class="form-control" placeholder="" value="{{ $VehicleData->VehicleType }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Seating">SEATING CAPACITY </label>
                                                        <input type="text" id="Seating" name="Seating" class="form-control" placeholder="" onkeypress="return isNumber(event)" value="{{ $VehicleData->Seating }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="MinFare">MINIMUM FARE ($) </label>
                                                        <input type="text" id="MinFare" name="MinFare" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->MinFare }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="BaseFare">BASE FARE ($) </label>
                                                        <input type="text" id="BaseFare" name="BaseFare" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->BaseFare }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="PricePerMinute">PRICE PER MINUTE ($) </label>
                                                        <input type="text" id="PricePerMinute" name="PricePerMinute" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->PricePerMinute }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="WaitingCharge">WAITING CHARGE PER MINUTE ($)</label>
                                                        <input type="text" id="WaitingCharge" name="WaitingCharge" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->WaitingCharge }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="PricePerMile">PRICE PER/MILE ($) </label>
                                                        <input type="text" id="PricePerMile" name="PricePerMile" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->PricePerMile }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="CancellationFee">CANCELATION FEE ($)</label>
                                                        <input type="text" id="CancellationFee" name="CancellationFee" class="form-control" onkeypress="return isNumber(event)" placeholder="" value="{{ $VehicleData->CancellationFee }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Description">VEHICLE TYPE DESCRIPTION</label>
                                                        <textarea class="form-control" rows="4" id="Description" name="Description">{{ $VehicleData->Description }}</textarea>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="seprator-block"></div>
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-info-outline mr-10"></i> Images </h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row" id="image_tier">
                                    
                                               
                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> VEHICLE ON IMAGE</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="OnImage" id="OnImage" onchange="uploadImage(this);">
                                                            </div>
                                                            <a href="{{ url($VehicleData->OnImage) }}" target="_blank">view</a>
                                                            <div>
                                                                <img src="" id="img" style="display: none;" height="200px" width="200px" class="image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> VEHICLE OFF IMAGE</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="OffImage" id="OffImage" onchange="uploadImage1(this);">
                                                            </div>
                                                            <a href="{{ url($VehicleData->OffImage) }}" target="_blank">view</a>
                                                            <div>
                                                                <img src="" id="img1" style="display: none;" height="200px" width="200px" class="image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> MAP ICON</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="MapIcon" id="MapIcon" onchange="uploadImage2(this);">
                                                            </div>
                                                            <a href="{{ url($VehicleData->MapIcon) }}" target="_blank">view</a>
                                                            <div>
                                                                <img src="" id="img2" style="display: none;" height="200px" width="200px" class=" image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="button" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <!-- /Row -->
</div>

<div class="modal fade" id="deleteVehicleImageModal" role="dialog">
    <div class="modal-dialog"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vehicle Image Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this vehicle Image ?</p>
            </div>
            <div class="modal-footer"> 
                <a href="javascript:void(0);" type="button" id="vehicle_image_delete_confirm_btn" class="btn btn-danger">Yes</a>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button> 
            </div> 
        </div> 
    </div>
</div>


<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
        
    </script>
    <script type="text/javascript">
        
        // Save order
        $('.btn-success').click(function(){

            var VehicleId       = $('#Id').val();
            var VehicleType     = $('#VehicleType').val();
            var Seating         = $('#Seating').val();
            var BaseFare        = $('#BaseFare').val();
            var MinFare         = $('#MinFare').val();
            var WaitingCharge   = $('#WaitingCharge').val();
            var CancellationFee = $('#CancellationFee').val();
            var PricePerMinute  = $('#PricePerMinute').val();
            var PricePerMile    = $('#PricePerMile').val();
            var Description     = $('#Description').val();
            var City            = $('#City').val();
            
            var OnImage         = $('#OnImage')[0].files[0];
            var OffImage        = $('#OffImage')[0].files[0];
            var MapIcon         = $('#MapIcon')[0].files[0];

            var css_error = ({
                'border-color': 'red'
            });
            var css_original = ({
                'border-color': 'rgb(204, 204, 204)'
            });

            $("#vehiclecategory,#brands,#lineas,#model,#state").css(css_original);

            if(VehicleType == ''){
                $("#vehiclecategory").focus();
                $("#vehiclecategory").css(css_error);
                return false;
            }else if(Seating == ''){
                $("#brands").focus();
                $("#brands").css(css_error);
                return false;
            }else if(BaseFare == ''){
                $("#lineas").focus();
                $("#lineas").css(css_error);
                return false;
            }else if(MinFare == ''){
                $("#model").focus();
                $("#model").css(css_error);
                return false;
            }else if(WaitingCharge == ''){
                $("#state").focus();
                $("#state").css(css_error);
                return false;
            }else if(CancellationFee == ''){
                $("#store").focus();
                $("#store").css(css_error);
                return false;
            }else if(PricePerMinute == ''){
                $("#files").focus();
                swal('Plase upload an image');
                return false;
            }else if(PricePerMile == ''){
                $("#files").focus();
                swal('Plase upload an image');
                return false;
            }else if(Description == ''){
                $("#files").focus();
                swal('Plase upload an image');
                return false;
            }else if(City == ''){
                $("#files").focus();
                swal('Plase upload an image');
                return false;
            }

            $('.btn-success').attr('disabled','disabled');
            $('.loader_div').show();

            var form_data = new FormData(); 
            form_data.append('Id',VehicleId);
            form_data.append('VehicleType',VehicleType);
            form_data.append('Seating',Seating);
            form_data.append('BaseFare',BaseFare);
            form_data.append('MinFare',MinFare);
            form_data.append('WaitingCharge',WaitingCharge);
            form_data.append('CancellationFee',CancellationFee);
            form_data.append('PricePerMinute',PricePerMinute);
            form_data.append('PricePerMile',PricePerMile);
            form_data.append('Description',Description);
            form_data.append('City',City);
            form_data.append('OnImage',OnImage);
            form_data.append('OffImage',OffImage);
            form_data.append('MapIcon',MapIcon);

            // AJAX request
            $.ajax({
                url: '../../vehicle/update',
                type: 'post',
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    window.location.href = '../../vehicle';
                }
            });
        });

    </script>
@endpush