<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
	public $timestamps = false;
	protected $primaryKey = 'UserId';
    protected $table = 'tbl_users';

    public function pushuser_data(){
    	return $this->hasMany(PushUser::class, 'UserId', 'UserId');
    }

}

