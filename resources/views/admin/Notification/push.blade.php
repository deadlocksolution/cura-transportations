@extends('layouts.admin_template')
@section('title', 'Empujar a todos')
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Empujar a todos</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <a href="{{ route('user') }}" class="btn btn-default pull-right">Go Back</a> -->
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('notification.store'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}
                                        <div class="form-body">

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="NotificationText"> Empujar texto *</label>
                                                        <input type="text" id="NotificationText" pattern=".*\S+.*" name="NotificationText" class="form-control" required="" placeholder="Ingresar Texto De Inserción" value="{{ old('NotificationText') }}">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Enviar empuje</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

