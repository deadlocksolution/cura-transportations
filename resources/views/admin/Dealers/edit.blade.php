@extends('layouts.admin_template')
@section('title', 'Update Distribuidores')
@section('main_container')

<style type="text/css">
    #exist_image_tier{
        margin:10px;
    }
    #exist_image_tier .img{
        height: 250px;
        width: 100%;
        background-size:contain !important;
        background-repeat: no-repeat !important;
        box-shadow: 1px 1px 5px 1px;
    }
    #exist_image_tier .delete_product_image_btn{
        float: left;
        position: absolute;
        top: -10px;
        right: 0;
        background:#fff;
    }

    .select2-container--default .select2-selection--single{
        border: none !important;
        height:auto !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        line-height: inherit !important;
    }
    .select2-container{
        height: 40px !important;
        margin-top: -1px !important;
    }
    .select2-container--default .select2-results>.select2-results__options{
        overflow-x: hidden;
        width: auto;
    }
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Update Distribuidores</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('dealer') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('dealer.update'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}
                                        <div class="form-body">

                                            <input type="hidden" id="UserId" name="UserId" value="{{ $DealerData->UserId}}">

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="FullName"> Nombre *</label>
                                                        <input type="text" id="FullName" pattern=".*\S+.*"  name="FullName" class="form-control" required="" placeholder="Enter Nombre" value="{{ $DealerData->FullName }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="email"> Email *</label>
                                                        <input type="email" pattern=".*\S+.*"  id="email" name="email" class="form-control" disabled placeholder="Enter Nombre" value="{{ $DealerData->Email }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="password"> Contraseña *</label>
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Nombre" value="">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="ContactNumber">Teléfono * </label>
                                                        <input type="text" id="ContactNumber" pattern=".*\S+.*" name="ContactNumber" class="form-control" placeholder="Enter Teléfono" value="{{ $DealerData->ContactNumber }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Facebook">Facebook</label>
                                                        <input type="text" id="Facebook" pattern=".*\S+.*" name="Facebook" class="form-control" placeholder="https://www.facebook.com" value="{{ $DealerData->Facebook }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Instagram">Instagram</label>
                                                        <input type="text" id="Instagram" pattern=".*\S+.*" name="Instagram" class="form-control" placeholder="https://www.instagram.com" value="{{ $DealerData->Instagram }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Whatsapp">Whatsapp</label>
                                                        <!-- <input type="text" id="Whatsapp" pattern=".*\S+.*" name="Whatsapp" class="form-control" placeholder="https://api.whatsapp.com/send?phone=99999999999&te" value="{{ $DealerData->Whatsapp }}"> -->
                                                        <div class="input-group"> 
                                                            <span class="input-group-addon" style="background: #cccccc5c;color: gray;">https://wa.me/</span>
                                                            <span class="input-group-addon" style="padding: 0;width: auto;">

                                                                <select class="form-control select2" name="CountryCode">
                                                                    @foreach($countryArr as $key=>$value)  
                                                                        <?= $isSelected = '';?>
                                                                        @if($DealerData->CountryCode == $value->PhoneCode)
                                                                             <?= $isSelected = 'selected';?>
                                                                        @endif
                                                                    <option <?= $isSelected?>>+{{ $value->PhoneCode }}</option> 
                                                                    @endforeach
                                                                </select>
        
                                                              
                                                            </span>
                                                            <input type="text" id="Whatsapp" pattern=".*\S+.*" name="Whatsapp" class="form-control" placeholder="Enter Whatsapp No" value="{{ $DealerData->Whatsapp }}" style="    margin-left: -1px;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="HelplineNo">Línea de ayuda no</label>
                                                        <input type="text" id="HelplineNo" pattern=".*\S+.*" name="HelplineNo" class="form-control" placeholder="Enter HelplineNo" value="{{ $DealerData->HelplineNo }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Website">Sitio web</label>
                                                        <input type="text" id="Website" pattern=".*\S+.*" name="Website" class="form-control" placeholder="Enter Website" value="{{ $DealerData->Website }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="ActiveVehicleCount">Cantidad de Productos Activos</label>
                                                        <input type="text" id="ActiveVehicleCount" pattern=".*\S+.*" name="ActiveVehicleCount" class="form-control" placeholder="Enter Cantidad de Productos Activos" value="{{ $DealerData->ActiveVehicleCount }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="OfferCount">Cantidad de Ofertas a la semana</label>
                                                        <input type="text" id="OfferCount" pattern=".*\S+.*" name="OfferCount" class="form-control" placeholder="Enter Cantidad de Ofertas a la semana" value="{{ $DealerData->OfferCount }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox mb-3">
                                                            <input type="checkbox" class="checkbox-custom" id="IsAddNewCar" name="IsAddNewCar" <?= ($DealerData->IsAddNewCar == 1)  ? 'checked' : '' ?>>
                                                            <label class="custom-control-label" for="customCheck">Publicar auto nuevo</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="seprator-block"></div>
                                                <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-info-outline mr-10"></i> Imágenes </h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row" id="image_tier">
                                                    <?php $DealerImages = json_decode($DealerData->ProfilePic,true);  ?>
                                                    <input type="hidden" class="image_file_arr_cnt" value="<?= count($DealerImages) ?>" name="image_file_arr_cnt">
                                                    <div class="col-md-12 row mt-10" id="image_block_1">
                                                        <div class="form-group">
                                                            <div class="col-md-10">
                                                                <img src="" id="vehicle_image_1" style="display: none;" height="200px" width="200px">
                                                                <div class="fileupload btn btn-info btn-anim">
                                                                    <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                    <input type="file" class="upload" name="image_file_arr[]" id="files" multiple>
                                                                </div>
                                                                <br><br><br>
                                                                <output id="list">
                                                                 <ul id="sortable" >
                                                                @if(count($DealerImages) > 0)
                                                                                
                                                                    <h5 class="text-left mt-40"> Imágenes existentes </h5>
                                                            
                                                                    <div class="row" id="exist_image_tier">
                                                                       
                                                                        @foreach($DealerImages AS $key => $Vehicle_image_value)
    
                                                                           <li class="ui-state-default existing_img" id="image_{{ $key }}">
                                                                                <img src="{{ url($Vehicle_image_value) }}" title="{{ $key }}">
                                                                                <span class="remove_img_preview" onclick="deleteDealerImageConfirmationModel({{ $DealerData->UserId }},{{ $key }});"></span>
                                                                            </li>
                                                                        @endforeach
                                                                        
                                                                    </div>
                                                                @endif
                                                                </ul>
                                                                </output>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions mt-10">
                                            <button type="button" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<div class="modal fade" id="deleteDealerImageModal" role="dialog">
    <div class="modal-dialog"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dealer Image Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this dealer Image ?</p>
            </div>
            <div class="modal-footer"> 
                <a href="javascript:void(0);" type="button" id="dealer_image_delete_confirm_btn" class="btn btn-danger">Yes</a>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button> 
            </div> 
        </div> 
    </div>
</div>

<!-- /page content -->
@endsection

@push('scripts')
<script type="text/javascript">
    $( "#sortable" ).sortable();

    // Save order
    $('.btn-success').click(function(){

        var name = $('#name').val();
        var email = $('#email').val();
        var Whatsapp = $('#Whatsapp').val();
        var address = $('#address').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var countryCode = $('.select2').val();
        var image = $('.image_file_arr_cnt').val();

        WhatsappNo = '';
        CountryCode = '';
        if(Whatsapp != ''){
            var CountryCode = countryCode.substring(1);
            var WhatsappNo = Whatsapp;
        }

        var css_error = ({
            'border-color': 'red'
        });
        var css_original = ({
            'border-color': 'rgb(204, 204, 204)'
        });

        $("#name,#email,#phone,#address,#latitude,#longitude").css(css_original);

        if(name == ''){
            $("#name").focus();
            $("#name").css(css_error);
            return false;
        }else if(email == '' || email == null){
            $("#email").focus();
            $("#email").css(css_error);
            return false;
        }else if(address == ''){
            $("#address").focus();
            $("#address").css(css_error);
            return false;
        }else if(latitude == ''){
            $("#latitude").focus();
            $("#latitude").css(css_error);
            return false;
        }else if(longitude == ''){
            $("#longitude").focus();
            $("#longitude").css(css_error);
            return false;
        }else if(image == 0){
            $("#files").focus();
            swal('Plase upload an image');
            return false;
        }

        $('.loader_div').show();
        
        var imageids_arr = [];
        // get image ids order
        $('#sortable li').each(function(){
            var id = $(this).attr('id');
            var split_id = id.split("_");
            var img = $(this).find("img").attr("src");
            if(img.indexOf('data:') != -1){
                imageids_arr.push(img.substring(5));
            }else{
                imageids_arr.push(img);
            }
            
        });

        var form_data = $('form').serializeArray();
        form_data.push({ name: "imageids", value: JSON.stringify(imageids_arr) });
        form_data.push({ name: "WhatsappNo", value: WhatsappNo });
        form_data.push({ name: "CountryCode", value: CountryCode });

        $.ajax({
            url: '../../dealer/update',
            type: 'post',
            data: form_data,
            success: function(response){
                var url = '../../dealer';
                window.location.href = url;
            }
        });
    });

    $('.select2').select2();

</script>
@endpush