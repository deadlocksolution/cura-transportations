<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class AdminController extends Controller {

	public function Login(Request $request) {
		// 		if (Auth::check()) {
		//     return Redirect::to('/admin/dashboard');
		// }
		return view('admin/auth/login');

	}

	public function PostLogin(Request $request) {

		$email = $request->input('email');
		$password = md5($request->input('password'));

		$data = User::select()
				->where(['Email' => $email])
				->where(['Password' => $password])
				->first();

		if (empty($data)) {
			return view('admin/auth/login', ["msg" => 'email and password are wrong.']);
		}
		if ($data->UserType == 1) {
			return view('admin/auth/login', ["msg" => 'email and password are wrong.']);
		}

		Session::put('AdminId', $data->UserId);
		Session::put('Email', $data->Email);
		Session::put('Type', $data->UserType);

		if($data->UserType == 0 || $data->UserType == '0'){
			return Redirect::to('user');	
		}else if($data->UserType == 3 || $data->UserType == '3'){
			// return Redirect::to('store/user');
			return Redirect::to('vehicle/auto/used');
		} else {
			return Redirect::to('dealer/stores');	
		}
		

	}

	public function Dashboard() {

		return view('admin/dashboard');

	}

	public function Logout() {

		Session::flush();
		// Session::forget('AdminId');
		// Session::forget('Email');

		return Redirect::to('/');

	}

	public function GetTermsCondition(){
		$terms = Admin::pluck('TermsAndCondition')[0];
		return view('admin/termscondition', compact('terms'));
	}

	public function SaveTermsCondition(Request $request){

		$terms = Admin::find(1);
		$terms->TermsAndCondition = (string)$request->TermsAndCondition;
		$terms->save();

		return response()->json(['success' => '1']);
	}

	public function Index(){
		$dataArr = Admin::select()->get();
        return view('admin.Config.index', compact('dataArr'));
	}

	public function Config(Request $request){

        $admin                      = Admin::find(Session::get('AdminId'));
        $admin->ActiveProductDays   = $request->ActiveProductDays;
        $admin->IsValidateToken     = $request->IsValidateToken;
        $admin->save();

        return 'true';
	}
}