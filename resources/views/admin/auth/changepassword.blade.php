<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title> CURA </title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content=""/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- vector map CSS -->
        <link href="{{ asset("public/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>

        <!-- Custom CSS -->
        <link href="{{ asset("public/dist/css/style.css") }}" rel="stylesheet" type="text/css">
    </head>

    <body>
        <!--Preloader-->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!--/Preloader-->

        <div class="wrapper  pa-0">
           <!--  <header class="sp-header">
                <div class="sp-logo-wrap pull-left">
                    <a href="{{ url('/login') }}">
                        <img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
                        <span class="brand-text">B2B Hardware</span>
                    </a>
                </div>
                <div class="form-group mb-0 pull-right">
                    <span class="inline-block pr-10">Don't have an account?</span>
                    <a class="inline-block btn btn-primary  btn-rounded btn-outline" href="{{ url('/signup') }}">Sign Up</a>
                </div>
                <div class="clearfix"></div>
            </header> -->

            <!-- Main Content -->
            <div class="page-wrapper pa-0 ma-0 auth-page">
                <div class="container-fluid">
                    <!-- Row -->
                    @if($Status == -1 || $Status == '-1')
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float" style="width: 800px;">
                                <h3 style="text-align: center;"> Your password has been changed successfully! Thank you.</h3>
                            </div>
                        </div>
                    </div>

                    @elseif($Status == 1 || $Status == '1')
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float">


                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="mb-30">
                                            <h3 class="text-center txt-dark mb-10">Reset Password</h3>
                                            <!-- <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6> -->
                                        </div>
                                        <div class="form-wrap">


                                            <form method="POST" action="{{ url('postchangepassword') }}" id="change_password">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="refkey" value="{{ $refkey }}">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="password">Password</label>
                                                    <input type="password" class="form-control" required="" id="password" name="password" placeholder="Enter Password" value="{{ old('password') }}">
                                                    <label id="pwd_error" style="color: red;margin-top: 20px; display: none;">The passwords you entered do not match. Please try again.</label>

                                                </div>

                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="confirm_password">Confirm Password</label>
                                                    <!-- <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="forgot-password.html">forgot password ?</a> -->
                                                    <div class="clearfix"></div>
                                                    <input type="password" class="form-control" required="" id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" value="{{ old('confirm_password') }}">
                                                    <label id="con_pwd_error" style="color: red;margin-top: 20px; display: none;">The passwords you entered do not match. Please try again.</label>

                                                </div>

                                                <label id="change_pwd_error" style="color: red;margin-top: 20px; display: none;"> password and confirm password you entered do not match. Please try again.</label>

                                                <div class="form-group text-center">
                                                    <button type="button" class="btn btn-primary"  onclick="return validateChangePassword()"> Change Password </button>
                                                </div>
                                            </form>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    @elseif($Status == 0 || $Status == '0')
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float" style="width: 800px;">
                                <h3 style="text-align: center;"> Link is expired. </h3>
                            </div>
                        </div>
                    </div>

                    @else
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float" style="width: 800px;">
                                <h3 style="text-align: center;">Something went wrong please try again. </h3>
                            </div>
                        </div>
                    </div>


                    @endif
                    <!-- /Row -->
                </div>

            </div>
            <!-- /Main Content -->

        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
        function validateChangePassword() {
            var password = $("#password").val();
            var confirm_password = $("#confirm_password").val();

            var isReturn = "true";

            var css_error = ({
                'border-color': 'red'
            });
            var css_original = ({
                'border-color': 'rgb(204, 204, 204)'
            });

            $("#password").css(css_original);
            $("#confirm_password").css(css_original);

            $('#pwd_error').hide();
            $('#con_pwd_error').hide();
            $('#change_pwd_error').hide();

            if (password == "") {
                $("#password").css(css_error);
                $("#pwd_error").text('Please enter password').show();
                isReturn = "false";
                return false;
            } else if (confirm_password == "") {
                $("#confirm_password").css(css_error);
                $("#con_pwd_error").text('Please enter confirm password').show();
                isReturn = "false";
                return false;
            }
            if (password != confirm_password) {
                $("#password").css(css_error);
                $("#confirm_password").css(css_error);
                $("#change_pwd_error").show();
                isReturn = "false";
                return false;
            }

            if (isReturn == "true") {
                $("#change_password").submit();
            }
        }
        </script>
        <!-- JavaScript -->

        <!-- jQuery -->
        <script src="{{ asset("public/vendors/bower_components/jquery/dist/jquery.min.js") }}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
        <script src="{{ asset("public/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js") }}"></script>

        <!-- Slimscroll JavaScript -->
        <script src="{{ asset("public/dist/js/jquery.slimscroll.js") }}"></script>

        <!-- Init JavaScript -->
        <script src="{{ asset("public/dist/js/init.js") }}"></script>
    </body>
</html>
