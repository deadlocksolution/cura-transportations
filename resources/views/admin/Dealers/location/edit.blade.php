@extends('layouts.admin_template')
@section('title', 'Update Location') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Update Location</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('dealer.location') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">

                                            @if (session()->has('flash_notification.message'))
                                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    {!! session('flash_notification.message') !!}
                                                </div>
                                            @endif

                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="form-wrap">
                                                        {{ Form::open(['route'=>['dealer.location.update'], 'enctype' => 'multipart/form-data', 'method'=>'post'])}} 
                                                        
                                                            <div class="form-body">

                                                                <input type="hidden" id="DealerlocationId" name="DealerlocationId" value="{{ $locationData->DealerlocationId }}">
                                                                
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="Address">Address *</label>
                                                                            <input type="text" class="form-control mb-20" name="Address" value="{{ $locationData->Address }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="Latitude">Latitude *</label>
                                                                            <input type="text" class="form-control mb-20" name="Latitude" value="{{ $locationData->Latitude }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="Longitude">Longitude *</label>
                                                                            <input type="text" class="form-control mb-20" name="Longitude" value="{{ $locationData->Longitude }}">
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                            <div class="form-actions mt-10">
                                                                <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                                            </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection
