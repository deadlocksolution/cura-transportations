<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleUtility extends Model {
	public $timestamps = false;
	protected $primaryKey = 'UtilityId';
    protected $table = 'tbl_vehicleutilities';
}

