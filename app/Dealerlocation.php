<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dealerlocation extends Model {
	public $timestamps = false;
	protected $primaryKey = 'DealerlocationId';
    protected $table = 'tbl_dealerlocation';
}

