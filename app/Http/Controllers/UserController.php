<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Dealerlocation;
use App\ApiSession;
use App\PushUser;
use App\Store;
use App\Vehicle;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;

class UserController extends Controller
{

    public function Index()
    {

        $dataArr = User::where('UserType','1')->orderBy("UserId", "ASC")->get();
        return view('admin.Users.index', compact('dataArr'));

    }

    public function Dealer()
    {

        $dataArr = User::where('UserType','2')->orderBy("UserId", "ASC")->get();
        return view('admin.Dealers.index', compact('dataArr'));

    }

    public function Create()
    {

        $countryArr = Country::get();
        return view('admin.Dealers.add', compact('countryArr'));

    }

    public function Store(Request $request)
    {

        
        if(isset($request->IsAddNewCar)) {
            $IsAddNewCar = 1; 
        }else{
            $IsAddNewCar = 0;
        }

        // $Image    = '';
        // $folderName = 'user_profile';
        // if ($request->hasFile('image')) {
        //     $file            = Input::file('image');
        //     $destinationPath = 'uploads/' . $folderName . '/';
        //     $file_name       = "delaer_" . time() . '_' . rand() . '.png';
        //     $file->move($destinationPath, $file_name);
        //     $profile_pic = 'uploads/' . $folderName . '/' . $file_name;
        //     $Image    = $profile_pic;       
        // }
        // $Image    = '';

        $folderName = 'user_profile';

        $ProfilePic = array();
        $image_cnt = $request->image_file_arr_cnt;
        $output_file ='uploads/' . $folderName . '/';
        $imageids = json_decode($request->imageids);


        $html = array();
        $i = 0;
        foreach ($imageids as $key => $value) {

            if(strpos($value, ';base64') !== false){ 
                $image = $this->base64_to_jpeg("data:".$value, $output_file);
                array_push($html,$image);
            }

            $i++;
        }

        $image = json_encode($html);

        $user                      = new User();
        $user->UserType            = 2;
        $user->FullName            = $request->name;
        $user->Email               = $request->email;
        // $user->Gender              = $request->gender;
        $user->ProfilePic          = $image;
        $user->Gender              = '';
        $user->ContactNumber       = $request->ContactNumber;
        $user->DateOfBirth         = date('Y-m-d H:i:s');
        $user->SocialId            = '';
        $user->SocialType          = '';
        $user->Password            = md5($request->password);
        $user->IsAddNewCar         = $IsAddNewCar;
        $user->Facebook            = (isset($request->Facebook) && $request->Facebook != '') ? $request->Facebook : '';
        $user->Instagram           = (isset($request->Instagram) && $request->Instagram != '') ? $request->Instagram : '';
        $user->Whatsapp            = (isset($request->WhatsappNo) && $request->WhatsappNo != '') ? $request->WhatsappNo : '';
        $user->CountryCode         = (isset($request->CountryCode) && $request->CountryCode != '') ? $request->CountryCode : '';
        $user->HelplineNo          = (isset($request->HelplineNo) && $request->HelplineNo != '') ? $request->HelplineNo : '';
        $user->Website             = (isset($request->Website) && $request->Website != '') ? $request->Website : '';
        $user->ActiveVehicleCount  = $request->ActiveVehicleCount;
        $user->OfferCount          = $request->OfferCount;
        $user->save();

        // flash()->success('User Inserted Successfully');
        // return Redirect::to('dealer/');
        return 'dealer/';

    }

    public function Edit($id)
    {
        $DealerData         = User::find($id);
        $countryArr = Country::get();
        return view('admin.Dealers.edit', compact('DealerData','countryArr'));
    }

    public function Update(Request $request)
    {

        if(isset($request->IsAddNewCar)) {
            $IsAddNewCar = 1; 
        }else{
            $IsAddNewCar = 0;
        }

        $id                             = $request->UserId;
        $user                           = User::find($id);
        $user->FullName                 = $request->FullName;
        $user->IsAddNewCar              = $IsAddNewCar;
        $user->ContactNumber            = $request->ContactNumber;

        $folderName = 'user_profile';

        $user->Facebook    = (isset($request->Facebook) && $request->Facebook != '') ? $request->Facebook : '';
        $user->Instagram   = (isset($request->Instagram) && $request->Instagram != '') ? $request->Instagram : '';
        $user->Whatsapp    = (isset($request->WhatsappNo) && $request->WhatsappNo != '') ? $request->WhatsappNo : '';
        $user->CountryCode    = (isset($request->CountryCode) && $request->CountryCode != '') ? $request->CountryCode : '';
        $user->HelplineNo  = (isset($request->HelplineNo) && $request->HelplineNo != '') ? $request->HelplineNo : '';
        $user->Website     = (isset($request->Website) && $request->Website != '') ? $request->Website : '';
        $user->ActiveVehicleCount= $request->ActiveVehicleCount;
        $user->OfferCount        = $request->OfferCount;
        $image_cnt = $request->image_file_arr_cnt;

        $output_file ='uploads/' . $folderName . '/';
        $imageids = json_decode($request->imageids);

        $html = array();
        $i = 0;
        foreach ($imageids as $key => $value) {
            if(strpos($value, ';base64') !== false){ 

                $image = $this->base64_to_jpeg("data:".$value, $output_file);
                array_push($html,$image);
                
            }else{
                array_push($html,$value);
            }

            $i++;
        }
        // $image = json_encode((object)$html);
        $image = json_encode($html);
        $user->ProfilePic = $image;

        if($request->password != ''){
            $user->Password      = md5($request->password);
        }
        $result = $user->save();

        // flash()->success('Dealer Updated Successfully');
        // return Redirect::to('dealer/'); 
        return 'dealer/';    

    }

    public function DeleteDealerImage(Request $request)
    {

        $DealerData = User::find($request->UserId);

        $Images    = json_decode($DealerData->ProfilePic, true);
        unset($Images[$request->ImageId]);

        $Images             = array_values($Images);
        $Images             = json_encode($Images);
        $dealer             = User::find($request->UserId);
        $dealer->ProfilePic = $Images;

        $result = $dealer->save();
    }

    public function Delete($id)
    {
        User::where('UserId', $id)->delete();

        $get_api_session = ApiSession::select()
        ->where('UserId', $id)
        ->get();

        // DELETE FROM SESSION

        if ($get_api_session) {
            foreach ($get_api_session as $value) {
                $SessionId = $value['SessionId'];
                $apisession = ApiSession::where('SessionId', $SessionId)->delete();
            }  
        }

        // DELETE FROM REGISER PUSH
        $get_push_user = PushUser::select()
                            ->where('UserId', $id)
                            ->get();

        if ($get_push_user) {
            foreach ($get_push_user as $value) {
                $UserId = $value['UserId'];
                $apisession = PushUser::where('UserId', $UserId)->delete();
            }  
        }

        // DELETE DEALER'S STORE
        Store::where('UserId', $id)->delete();

        // DELETE DEALER'S STORE USER
        User::where('DealerId', $id)->delete();

        // DELETE DEALER'S VEHICLE
        Vehicle::where('DealerId', $id)->delete();

        echo 'success';
        exit();
    }

    public function ChangePassword($refkey)
    {
        $refkey = base64_decode($refkey);
        $refkey = explode('&', $refkey);
        $check  = User::where('RefKey', $refkey[1])->get();

        if ($check->count() > 0) {
            return view('admin.auth.changepassword', ['refkey' => $refkey[1], 'Status' => 1]);
        } else {
            return view('admin.auth.changepassword', ['refkey' => $refkey[1], 'Status' => 0]);
        }
    }

    public function PostChangePassword(Request $request)
    {

        $check = User::where('RefKey', $request->refkey)->first();

        $id = $check->UserId;

        $user = User::find($id);
        $user->RefKey = '';
        $user->Password = md5($request->password);
        $user->save();

        return view('admin.auth.changepassword', ['refkey' => '', 'Status' => -1]);
    }

    // DEALER LOCATION

    public function DealerLocation()
    {
        $dataArr = DealerLocation::where('UserId',Session::get('AdminId'))->get();
        return view('admin.Dealers.location.index',compact('dataArr'));
    }

    public function DealerLocationCreate()
    {

        return view('admin.Dealers.location.add');

    }

    public function DealerLocationStore(Request $request)
    {

        $location                = new DealerLocation();
        $location->UserId        = Session::get('AdminId');
        $location->Address       = $request->Address;
        $location->Latitude      = $request->Latitude;
        $location->Longitude     = $request->Longitude;
        $location->save();

        flash()->success('User Inserted Successfully');
        return Redirect::to('dealer/location/');

    }

    public function DealerLocationEdit($id)
    {
        $locationData         = DealerLocation::find($id);
        return view('admin.Dealers.location.edit', compact('locationData'));
    }

    public function DealerLocationUpdate(Request $request)
    {

        $id                            = $request->DealerlocationId;
        $Dealerlocation                = DealerLocation::find($id);
        $Dealerlocation->Address       = $request->Address;
        $Dealerlocation->Latitude      = $request->Latitude;
        $Dealerlocation->Longitude     = $request->Longitude;

        $result = $Dealerlocation->save();

        flash()->success('Location Updated Successfully');

        return Redirect::to('dealer/location/');    

    }

    public function DealerLocationDestroy($id)
    {

        DealerLocation::where('DealerlocationId', $id)->delete();
        echo 'success';
        exit();
    }

    public function CheckUniqueEmail(Request $request)
    {

        $data = User::where('Email', $request->email)->get();

        if(count($data) > 0){
            return "false";
        }else{
            return "true";
        }

    }

    public function base64_to_jpeg($base64_string, $output_file) {

        $image_parts = explode(";base64,", $base64_string);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file =  $output_file ."user_profile_" . time() . '_' . rand() . '.png';
        file_put_contents($file, $image_base64);

        return $file;
    }

}
