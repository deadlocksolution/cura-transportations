@extends('layouts.admin_template')
@section('title', 'Brands List')
@section('main_container')
@php $segment = Request::segment(3); @endphp
<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Marcas</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('brand.create') }}" class="btn btn-primary pull-right">Añadir marca</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="{{ ($segment == 'auto') ? 'active' : '' }}"  >
                            <a href="{{ route('brand',array('auto')) }}" > Automóvil </a>
                        </li>
                        <li role="presentation" class="{{ ($segment == 'motorcycle') ? 'active' : '' }}">
                            <a href="{{ route('brand',array('motorcycle')) }}" > Motocicleta </a>
                        </li>
                    </ul>

                    <div class="panel-body">
                        <div class="table-wrap">

                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif

                            <div class="table-responsive">

                                <table id="data_table" class="table table-hover display pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Código de marca</th>
                                            <th>Nombre de la marca</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->BrandId }}" id="record_{{ $data_value->BrandId }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    <td>{{ $data_value->BrandCode }}</td>
                                                    <td>{{ $data_value->Name }}</td>
                                                    <td>{{ ($data_value->Status == 1) ? "Active" : "Locked" }}</td>

                                                    <td>
                                                        <a href="{{ route('brand.edit', array($data_value->BrandId)) }}">
                                                            <i class="fa fa-pencil-square-o fa-2x"></i>
                                                        </a>&nbsp;&nbsp;

                                                        <a href="javascript:void(0);" onclick="deletePopup('../../brand/delete/', {{ $data_value->BrandId }})">
                                                            <i class="fa fa-trash-o fa-2x"></i>
                                                        </a>
                                                    </td>
                                                </tr>

                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 5 ] }],
                // "aaSorting": [[4,'DESC']]
            });
        });
    </script>
@endpush
