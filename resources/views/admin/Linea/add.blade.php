@extends('layouts.admin_template')
@section('title', 'Add Brands') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Añadir Linea</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('linea',array('auto')) }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('linea.store'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}

                                        <div class="form-body">    
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="code">Código linea *</label>
                                                        <input type="text" id="code" pattern=".*\S+.*" name="code" class="form-control" required="" placeholder="Enter Code">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="name">Nombre de linea *</label>
                                                        <input type="text" id="name" pattern=".*\S+.*" name="name" class="form-control" required="" placeholder="Enter Name">
                                                    </div>
                                                </div>
                                                    

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="status">Status</label>
                                                        <select class="form-control" name="status" id="status"  required="">
                                                            <option value="1">Active</option>
                                                            <option value="0">Locked</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="vehicletype">Tipo</label>
                                                        <select class="form-control" name="vehicletype" id="vehicletype"  required="" onchange="VehicleChange()">
                                                            <option value="1">Automóvil</option>
                                                            <option value="2">Motocicleta</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="brand">Marca</label>
                                                        <select class="form-control" name="brand" id="brand" required="" >
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="category">Categoría</label>
                                                        <select class="form-control select2 select2-multiple" multiple="multiple" name="category[]" id="category" required="" >
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $(".select2").select2();
});
VehicleChange();

function VehicleChange(){
    var type  = $('#vehicletype').val();

    $.ajax({
        type: "POST",
        url: '../getcategory',
        data: {
            type: type
        },
        success: function (data) {
            var obj = JSON.parse(data);
            var category = obj.category;
            var brand = obj.brand;


            $('#category').prop('disabled', false).html('').append('<option value="">Select Category</option>');
            $('#brand').prop('disabled', false).html('').append('<option value="">Select Marca</option>');
            for (var i = 0; i < category.length; i++) {
                $('#category').append('<option value=' + category[i].VehicleCategoryId + '>' + category[i].VehicleName + '</option>');
            }
            for (var i = 0; i < brand.length; i++) {
                $('#brand').append('<option value=' + brand[i].BrandId + '>' + brand[i].Name + '</option>');
            }
           
        }
    });


}
</script>
@endpush