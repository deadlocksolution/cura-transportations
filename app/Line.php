<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line extends Model {
	public $timestamps = false;
	protected $primaryKey = 'LineId';
    protected $table = 'tbl_lines';

    public function brand_data() {
        return $this->hasOne( Brand::class, 'BrandId', 'BrandId');
    }

    public function category_data() {
        return $this->hasOne( VehicleCategory::class, 'VehicleCategoryId', 'VehicleCategoryId');
    }

}

