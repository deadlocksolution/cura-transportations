<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
// Event::listen('illuminate.query', function ($query) {
//     var_dump($query);
// });

// Route::get('/', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
 */
Route::get('test_api', function () {
    return view('api/test_api');
});
Route::post('/api', 'ApiController@ApiRequest');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
 */

Route::get('/', 'AdminController@Login');
Route::get('/login', 'AdminController@Login')->name('login');
Route::post('/postlogin', 'AdminController@PostLogin');
Route::get('/logout', 'AdminController@Logout')->name('logout');

Route::get('configuration', 'AdminController@Index')->name('configuration');
Route::post('configuration/update', 'AdminController@Config')->name('configuration.update');


Route::get('/changepassword/{refkey}', 'UserController@ChangePassword')->name('changepassword');
Route::post('/postchangepassword', 'UserController@PostChangePassword')->name('post_changepassword');

Route::post('/user/CheckUniqueEmail', 'UserController@CheckUniqueEmail')->name('CheckUniqueEmail');


Route::get('dashboard', 'AdminController@Dashboard')->name('dashboard');
Route::get('user', 'UserController@Index')->name('user');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('user.delete');
Route::get('dealer', 'UserController@Dealer')->name('dealer');
Route::get('dealer/create', 'UserController@Create')->name('dealer.create');
Route::post('dealer/store', 'UserController@Store')->name('dealer.store');
Route::get('dealer/{id}/edit', 'UserController@Edit')->name('dealer.edit');
Route::post('dealer/update', 'UserController@Update')->name('dealer.update');
Route::get('/dealer/delete/{id}', 'UserController@Delete')->name('dealer.delete');
Route::post('dealer/{id}/delete_dealer_image', 'UserController@DeleteDealerImage')->name('dealer.delete_dealer_image');

/*
|--------------------------------------------------------------------------
| Admin Dealer location Routes
|--------------------------------------------------------------------------
 */

Route::get('dealer/location', 'UserController@DealerLocation')->name('dealer.location');
Route::post('dealer/location/store', 'UserController@DealerLocationStore')->name('dealer.location.store');
Route::get('dealer/location/create', 'UserController@DealerLocationCreate')->name('dealer.location.create');
Route::get('dealer/location/{id}/edit', 'UserController@DealerLocationEdit')->name('dealer.location.edit');
Route::post('dealer/location/update', 'UserController@DealerLocationUpdate')->name('dealer.location.update');
Route::get('dealer/location/delete/{id}', 'UserController@DealerLocationDestroy')->name('dealer.location.delete');

/*
|--------------------------------------------------------------------------
| Admin Vehicle Routes
|--------------------------------------------------------------------------
 */

Route::get('vehicle/', 'VehicleController@Index')->name('vehicle');
Route::get('vehicle/create', 'VehicleController@Create')->name('vehicle.create');
Route::post('vehicle/store', 'VehicleController@Store')->name('vehicle.store');
Route::get('vehicle/{id}/edit', 'VehicleController@Edit')->name('vehicle.edit');
Route::post('vehicle/update', 'VehicleController@Update')->name('vehicle.update');
Route::get('vehicle/delete/{id}', 'VehicleController@Destroy')->name('vehicle.delete');


/*
|--------------------------------------------------------------------------
| Admin Vehicle Category Routes
|--------------------------------------------------------------------------
 */
Route::get('category/type/{type}', 'CategoryController@Index')->name('category');
Route::get('category/create', 'CategoryController@Create')->name('category.create');
Route::post('category/store', 'CategoryController@Store')->name('category.store');
Route::get('category/{id}/edit', 'CategoryController@Edit')->name('category.edit');
Route::post('category/update', 'CategoryController@Update')->name('category.update');
Route::get('category/delete/{id}', 'CategoryController@Destroy')->name('category.delete');

/*
|--------------------------------------------------------------------------
| Admin Vehicle Brands Routes
|--------------------------------------------------------------------------
 */

Route::get('brand/type/{type}', 'BrandController@Index')->name('brand');
Route::get('brand/create', 'BrandController@Create')->name('brand.create');
Route::post('brand/store', 'BrandController@Store')->name('brand.store');
Route::get('brand/{id}/edit', 'BrandController@Edit')->name('brand.edit');
Route::post('brand/update', 'BrandController@Update')->name('brand.update');
Route::get('brand/delete/{id}', 'BrandController@Destroy')->name('brand.delete');


/*
|--------------------------------------------------------------------------
| Admin Notification Routes
|--------------------------------------------------------------------------
 */

Route::get('push-all', 'NotificationController@Index')->name('push-all');
Route::post('push/store', 'NotificationController@Store')->name('notification.store');

Route::get('push-location', 'NotificationController@Location')->name('push-location');
Route::post('push-location/add', 'NotificationController@Add')->name('notification.add');
Route::get('push-location/create', 'NotificationController@Create')->name('notification.create');
Route::get('push-location/{id}/edit', 'NotificationController@Edit')->name('notification.edit');
Route::post('push-location/update', 'NotificationController@Update')->name('notification.update');
Route::get('push-location/delete/{id}', 'NotificationController@Destroy')->name('notification.delete');

/*
|--------------------------------------------------------------------------
| Notifiaction And Reminder
|--------------------------------------------------------------------------
 */

Route::get('/PostExpirationReminder', 'NotificationController@PostExpirationReminder');
Route::get('/SendDealOftheWeekReminderToDealer', 'NotificationController@SendDealOftheWeekReminderToDealer');
Route::get('/SendDealOftheWeekReminderToUser', 'NotificationController@SendDealOftheWeekReminderToUser');
Route::get('/ResetOffers', 'NotificationController@ResetOffers');
Route::get('/CheckPostExpirationDate', 'NotificationController@CheckPostExpirationDate');

Route::get('/push', 'Push@Push');
Route::get('/SendDealOftheWeekReminderToDealer-test', 'Push@SendDealOftheWeekReminderToDealer');
Route::get('/SendDealOftheWeekReminderToUser-test', 'Push@SendDealOftheWeekReminderToUser');

/*
|--------------------------------------------------------------------------
| Terms and condition Routes
|--------------------------------------------------------------------------
 */

Route::get('terms', 'AdminController@GetTermsCondition')->name('terms');
Route::post('terms/store', 'AdminController@SaveTermsCondition')->name('terms.store');

Route::get('/test', 'NotificationController@test');


// --------------------------
/*
|--------------------------------------------------------------------------
| Admin Vehicle Brands Routes
|--------------------------------------------------------------------------
 */

Route::get('country', 'CountryController@Index')->name('country');
Route::get('city/create', 'CountryController@Create')->name('city.create');
Route::post('city/store', 'CountryController@StoreCity')->name('city.store');
Route::post('country/store', 'CountryController@Store')->name('country.store');
Route::get('country/get_city/{id}', 'CountryController@GetCity')->name('country.get_city');
Route::post('country/activate_city', 'CountryController@ActivateCity')->name('country.activate_city');


Route::get('country/{id}/edit', 'CountryController@Edit')->name('country.edit');
Route::post('country/update', 'CountryController@Update')->name('country.update');
Route::get('country/delete/{id}', 'CountryController@Destroy')->name('country.delete');