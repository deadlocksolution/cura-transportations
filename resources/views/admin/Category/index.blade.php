@extends('layouts.admin_template')
@section('title', 'Category List') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Category List</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('category.create') }}" class="btn btn-primary pull-right">Add Category</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Category Logo</th>
                                            <th>Description</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Category Name</th>
                                            <th>Category Logo</th>
                                            <th>Description</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->id }}" id="record_{{ $data_value->id }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    <td>{{ $data_value->name }}</td>
                                                    <td>
                                                        @if($data_value->logo != "")
                                                        <img src="{{ asset("public/images/category") . "/" . $data_value->logo }}" width="70px" height="70px">
                                                        @endif
                                                    </td>
                                                    <td>{{ $data_value->description }}</td>
                                                    <td>{{ $data_value->created_at }}</td>
                                                    <td>
                                                        <a href="{{ route('category.edit', array($data_value->id)) }}">
                                                            <i class="fa fa-pencil-square-o fa-2x"></i> 
                                                        </a>&nbsp;&nbsp;   
                                                    
                                                        <a href="javascript:void(0);" onclick="deletePopup('category/delete/', {{ $data_value->id }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 5 ] }],
                "aaSorting": [[4,'DESC']]
            });  
        });  
    </script>
@endpush