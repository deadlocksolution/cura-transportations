@extends('layouts.admin_template')
@section('title', 'Vehicle List') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Vehicle Type</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('vehicle.create')}}" class="btn btn-primary pull-right">Add</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    
                    
                    
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>TYPE ID</th>
                                            <th>TYPE NAME</th>
                                            <th>MAX SIZE</th>
                                            <th>BASE FARE</th>
                                            <th>MIN FARE</th>
                                            <th>WAITING CHARGE/MINUTE</th>
                                            <th>CANCELLATION FEE</th>
                                            <th>PRICE/MINUTE ($)</th>
                                            <th>PRICE/MILE ($)</th>
                                            <th>TYPE DESCRIPTION</th>
                                            <th>CITY NAME</th>
                                            <th>ON IMAGE</th>
                                            <th>OFF IMAGE</th>
                                            <th>MAP ICON</th>
                                            <th>ACTION</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->VehicleId }}" id="record_{{ $data_value->VehicleId }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    <td>{{ $data_value->VehicleType }}</td>
                                                    <td>{{ $data_value->Seating }}</td>
                                                    <td>{{ $data_value->BaseFare }}</td>
                                                    <td>{{ $data_value->MinFare }}</td>
                                                    <td>{{ $data_value->WaitingCharge }}</td>
                                                    <td>{{ $data_value->CancellationFee }}</td>
                                                    <td>{{ $data_value->PricePerMinute }}</td>
                                                    <td>{{ $data_value->PricePerMile }}</td>
                                                    <td>{{ $data_value->Description  }}</td>
                                                    <td>{{ $data_value->City }}</td>
                                                    <td>
                                                        @if(!empty($data_value->OnImage))
                                                            @php
                                                                $image = $data_value->OnImage;
                                                                if(!empty($image)){
                                                                    $url = url($image);
                                                                } else {
                                                                     $url = '';
                                                                }

                                                            @endphp
                                                            <img src=" {{ $url }}" alt="images" style="width: 100px;">

                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($data_value->OffImage))
                                                            @php
                                                                $image = $data_value->OffImage;
                                                                if(!empty($image)){
                                                                    $url = url($image);
                                                                } else {
                                                                     $url = '';
                                                                }

                                                            @endphp
                                                            <img src=" {{ $url }}" alt="images" style="width: 100px;">

                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!empty($data_value->MapIcon))
                                                            @php
                                                                $image = $data_value->MapIcon;
                                                                if(!empty($image)){
                                                                    $url = url($image);
                                                                } else {
                                                                     $url = '';
                                                                }

                                                            @endphp
                                                            <img src=" {{ $url }}" alt="images" style="width: 100px;">

                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('vehicle.edit', array($data_value->VehicleId)) }}">
                                                            <i class="fa fa-pencil-square-o fa-2x"></i> 
                                                        </a>&nbsp;&nbsp;

                                                        <a href="javascript:void(0);" onclick="deletePopup('vehicle/delete/', {{ $data_value->VehicleId }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 5 ] }],
                "aaSorting": [[4,'DESC']]
            });
            
            $(".select2").select2();
        });
    </script>
@endpush