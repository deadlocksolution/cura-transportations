<?php
namespace App\Http\Controllers;

use App\Brand;
use App\Currency;
use App\Http\Controllers\Controller;
use App\User;
use App\Line;
use App\Vehicle;
use App\VehicleCategory;
use App\UserFilter;
use App\Favourite;
use App\Notification;
use App\SentPushLog;
use App\Config;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use Illuminate\Support\Facades\Storage;

class VehicleController extends Controller
{

    public function Index()
    {

        $dataArr = Vehicle::where(['status' => 1])
                            ->orderBy("VehicleId", "DESC")
                            ->get();

        return view('admin.Vehicles.index', compact('dataArr'));
    }

    public function Create()
    {
      
        return view('admin.Vehicles.add');

    }
    public function Store(Request $request)
    {

        $folderName = 'vehicle';

        $OnImage    = '';
        if ($request->hasFile('OnImage')) {

            $file            = Input::file('OnImage');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    = 'uploads/' . $folderName . '/' . $file_name;    
            $OnImage = $Image;
        }
        $OffImage    = '';
        if ($request->hasFile('OffImage')) {

            $file            = Input::file('OffImage');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    =  'uploads/' . $folderName . '/' . $file_name;  
            $OffImage = $Image;
        }
        $MapIcon    = '';
        if ($request->hasFile('MapIcon')) {

            $file            = Input::file('MapIcon');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    = 'uploads/' . $folderName . '/' . $file_name;     
            $MapIcon = $Image;
        }

        $vehicle                    = new Vehicle();
        $vehicle->VehicleType       = $request->VehicleType;
        $vehicle->Seating           = $request->Seating;
        $vehicle->BaseFare          = $request->BaseFare;
        $vehicle->MinFare           = $request->MinFare;
        $vehicle->WaitingCharge     = $request->WaitingCharge;
        $vehicle->CancellationFee   = $request->CancellationFee;
        $vehicle->PricePerMinute    = $request->PricePerMinute;
        $vehicle->PricePerMile      = $request->PricePerMile;
        $vehicle->Description       = $request->Description;
        $vehicle->City              = 'city';
        $vehicle->OnImage           = $OnImage;
        $vehicle->OffImage          = $OffImage;
        $vehicle->MapIcon           = $MapIcon;
        $vehicle->save();

        return Redirect::to('vehicle/');
        
    }

    public function Edit($id)
    {
        $VehicleData         = Vehicle::find($id);
        return view('admin.Vehicles.edit', compact('VehicleData'));
    }

    public function Update(Request $request)
    {

        $folderName = 'vehicle';
        if ($request->hasFile('OnImage')) {

            $file            = Input::file('OnImage');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    = 'uploads/' . $folderName . '/' . $file_name;    
            $OnImage = $Image;
        }

        if ($request->hasFile('OffImage')) {

            $file            = Input::file('OffImage');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    =  'uploads/' . $folderName . '/' . $file_name;  
            $OffImage = $Image;
        }
        if ($request->hasFile('MapIcon')) {

            $file            = Input::file('MapIcon');
            $destinationPath = 'uploads/' . $folderName . '/';
            $file_name       = "vehicle_" . time() . '_' . rand() . '.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $file_name);
            $Image    = 'uploads/' . $folderName . '/' . $file_name;     
            $MapIcon = $Image;
        }

        $id                         = $request->Id;
        $vehicle                    = Vehicle::find($id);
        $vehicle->VehicleType       = $request->VehicleType;
        $vehicle->Seating           = $request->Seating;
        $vehicle->BaseFare          = $request->BaseFare;
        $vehicle->MinFare           = $request->MinFare;
        $vehicle->WaitingCharge     = $request->WaitingCharge;
        $vehicle->CancellationFee   = $request->CancellationFee;
        $vehicle->PricePerMinute    = $request->PricePerMinute;
        $vehicle->PricePerMile      = $request->PricePerMile;
        $vehicle->Description       = $request->Description;
        $vehicle->City              = 'city';
        if($request->hasFile('OnImage')){
            $vehicle->OnImage       = $OnImage;
        }
        if($request->hasFile('OffImage')){
            $vehicle->OffImage      = $OffImage;
        }
        if($request->hasFile('MapIcon')){
            $vehicle->MapIcon       = $MapIcon;
        }
        $result = $vehicle->save();

        return Redirect::back();

    }


    public function Destroy($id)
    {

        Vehicle::where('VehicleId', $id)->delete();
        echo 'success';
        exit();
    }

    public function VehicleImages($id){

        $VehicleData = Vehicle::where('VehicleId',$id)->pluck('VehicleImages');
        return $VehicleData;
    }

}
