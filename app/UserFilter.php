<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFilter extends Model {
	public $timestamps = false;
	protected $primaryKey = 'UserFilterId';
    protected $table = 'tbl_userfilters';

    public function userfilter_data()
    {
        return $this->hasOne(Brand::class, 'UserFilterId', 'UserFilterId');
    }

    public function brand_data(){
    	return $this->hasOne(Brand::class, 'BrandId', 'BrandId');	
    }

    public function line_data(){
    	return $this->hasOne(Line::class, 'LineId', 'LineId');	
    }

}

