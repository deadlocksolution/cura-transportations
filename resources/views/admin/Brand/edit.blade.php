@extends('layouts.admin_template')
@section('title', 'Update Brands')
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Actualizar marcas</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('brand',array('auto')) }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['route'=>['brand.update'], 'enctype' => 'multipart/form-data', 'method'=>'post'])}}
                                        <div class="form-body">
                                            <input type="hidden" name="id" value="{{ $BrandData->BrandId }}">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="name">Código de marca *</label>
                                                        <input type="text" id="code" name="code" class="form-control" required="" readonly="" placeholder="Enter Code" value="{{ $BrandData->BrandCode }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="name">Nombre de la marca *</label>
                                                        <input type="text" id="name" name="name" class="form-control" required="" placeholder="Enter Name" value="{{ $BrandData->Name }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="status">Status</label>
                                                        <select class="form-control" name="status" id="status">
                                                            <option value="1" <?php echo (isset($BrandData->Status) && $BrandData->Status == 1) ? 'selected="selected"' : ""; ?>>Active</option>
                                                            <option value="0" <?php echo (isset($BrandData->Status) && $BrandData->Status == 0) ? 'selected="selected"' : ""; ?>>Locked</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="vehicletype">Tipo</label>
                                                        <select class="form-control" name="vehicletype" id="vehicletype" onchange="VehicleType(this.value)">
                                                            <option value="1" <?php echo (isset($BrandData->VehicleType) && $BrandData->VehicleType == 1) ? 'selected="selected"' : ""; ?> >Automóvil</option>
                                                            <option value="2" <?php echo (isset($BrandData->VehicleType) && $BrandData->VehicleType == 2) ? 'selected="selected"' : ""; ?>>Motocicleta</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="vehiclecategories">Categoría de vehículo</label>
                                                        <select class="form-control select2 select2-multiple" required="" multiple="multiple" name="vehiclecategories[]" id="vehiclecategories" multiple="" >
                                                        </select>
                                                    </div>
                                                </div>





                                            </div>
                                        </div>

                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
        VehicleType({{ $BrandData->VehicleType }} );
        function VehicleType(type){

            var categoryData =<?= json_encode(explode(',', $BrandData->VehicleCategories )); ?>;
            
            $.ajax({
                type: "GET",
                url: "../../vehicle/get_vehicle_categories/"+type,
                data: {
                    // 'type' : type
                },
                success: function (res) {
                    var obj = JSON.parse(res);

                    $('#vehiclecategories').prop('disabled', false).html('').append('<option value="">Seleccione uno </option>');
                    for (var i = 0; i < obj.length; i++) {
                        var selected = '';
                        if (jQuery.inArray(String(obj[i].VehicleCategoryId), categoryData) != '-1') {
                            selected = 'selected';
                        } 

                        $('#vehiclecategories').append('<option value=' + obj[i].VehicleCategoryId + ' ' + selected + '>' + obj[i].VehicleName + '</option>');
                    }
                }
            });
        }


    </script>
@endpush