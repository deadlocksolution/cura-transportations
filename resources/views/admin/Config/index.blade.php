@extends('layouts.admin_template')
@section('title', 'Configuration ') 
@section('main_container')

<style type="text/css">
    .form-group button {
        padding: 6px 30px;
        margin-right: 16px;
        background: #ddd;
        font-size: 17px;
        border: none;
        cursor: unset;
        position: absolute;
        right: 0;
        height: 40px;
        top: 32px;
    }

</style>

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Configuration</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <a href="#" class="btn btn-primary pull-right">Add City</a> -->
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    
                    
                    
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                   {{ Form::open(['url' => route('configuration.update'), 'role'=>'form']) }}
                                        <div class="form-body">
                                            
                                            <div class="row">
                                              
                                                <div class="col-md-12">
                                                    <div class="form-group col-md-6">
                                                        <label class="control-label mb-10" for="ActiveProductDays">Días de Producto Activo </label>
                                                        <input type="text" id="ActiveProductDays" pattern=".*\S+.*" name="ActiveProductDays" class="form-control"  placeholder="Enter Días de Producto Activo" value="{{ $dataArr[0]->ActiveProductDays }}">
                                                        <button type="button">Dias</button>

                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group IsValidateToken col-md-6">
                                                        <label class="control-label mb-10" for="IsValidateToken">Validación de Token</label>
                                                        <select class="form-control" name="IsValidateToken" id="IsValidateToken" >
                                                            <option value=""> Seleccione uno  </option>
                                                            <option value="1" <?php echo (isset($dataArr[0]->IsValidateToken) && $dataArr[0]->IsValidateToken == 1) ? 'selected="selected"' : ""; ?>> Si </option>
                                                            <option value="0" <?php echo (isset($dataArr[0]->IsValidateToken) && $dataArr[0]->IsValidateToken == 0) ? 'selected="selected"' : ""; ?>> No </option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="button" class="btn btn-success mr-10 btn-success"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $('.btn-success').click(function(){

            var ActiveProductDays = $('#ActiveProductDays').val();
            var IsValidateToken = $('#IsValidateToken').val();
           
            var css_error = ({
                'border-color': 'red'
            });
            var css_original = ({
                'border-color': 'rgb(204, 204, 204)'
            });

            $("#ActiveProductDays,#IsValidateToken").css(css_original);

            if(ActiveProductDays == ''){
                $("#ActiveProductDays").focus();
                $("#ActiveProductDays").css(css_error);
                return false;
            }else if(IsValidateToken == '' || IsValidateToken == null){
                $("#IsValidateToken").focus();
                $("#IsValidateToken").css(css_error);
                return false;
            }

            $('.loader_div').show();

            var form_data = $('form').serializeArray();

            // AJAX request
            $.ajax({
                url: 'configuration/update',
                type: 'post',
                data: form_data,
                success: function(response){
                    $('.loader_div').hide();
                    swal('swal','Configuración exitosa');
                }
            });
        });
    </script>
@endpush