<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSession extends Model {
	public $timestamps = false;
	protected $primaryKey = 'SessionId';
    protected $table = 'tbl_apisession';
}

