@extends('layouts.admin_template')
@section('title', 'Users List') 
@section('main_container')

<style type="text/css">

</style>

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Usuarios</h5>
        </div>
       
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    
                    
                    
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Usuario</th>
                                            <th>Nombre</th>
                                            <th>Email</th>
                                            <!-- <th>Número de contacto</th> -->
                                            <th>Género</th>
                                            <th>Creado en</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>State Name</th>
                                            <th>City Name</th>
                                            <th>Status</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                                        
                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->UserId }}" id="record_{{ $data_value->UserId }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    <td>{{ $data_value->UserName }}</td>
                                                    <td>{{ $data_value->FullName }}</td>
                                                    <td>{{ $data_value->Email }}</td>
                                                    <!-- <td>{{ $data_value->ContactNumber }}</td> -->
                                                    <td>{{ ($data_value->Gender == 1) ? "Male" : "Female" }}</td>
                                                    <td>{{ $data_value->CreatedAt }}</td>
                                                    <td>
                                                     
                                                        <a href="javascript:void(0);" onclick="deletePopup('user/delete/', {{ $data_value->UserId }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 5 ] }],
                "aaSorting": [[4,'DESC']]
            });
            
            $(".select2").select2();
        });
    </script>
    <script type="text/javascript">
        function setSafeCheck(UserId){
            $.ajax({
                type: "POST",
                url: 'vehicle/SafeCheck',
                data: {
                      'UserId': UserId
                },
                success: function (res) {
                }
            });
        }
    </script>
@endpush