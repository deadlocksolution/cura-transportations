<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Notificationsolog;
use App\SentPushLog;
use App\User;
use App\Vehicle;
use App\PushUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;

class NotificationController extends Controller
{

    public function Index()
    {
        return view('admin.Notification.push');
    }

    public function Location()
    {
        $dataArr = Notification::select()
            ->where('NotificationType', '2')
            ->orderBy("NotificationId", "DESC")
            ->get();

        return view('admin.Notification.pushByLocation', compact('dataArr'));
    }

    public function Store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'NotificationText' => 'required',
            ]
        );

        $notificationType = '1';
        $message          = $request->NotificationText;

        $dataArr = User::where('UserType', '1')
            ->Join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_users.UserId')
            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'tbl_users.UserId')
            ->where('ns.AllPush', 1)
            ->where('tbl_users.UserType', '!=' , 2)
            // ->where('pu.DeviceToken' , 'eP_LFwSxpus:APA91bGJNHyfIv49ob8N9u6EezqAxy2rUhug5OKScw83HHuYVKV0SNe_3tU76JLQB6X85S3ziXKArbJMKRVpw-oTsetJTkU_BlV_NcWj9DJIZLI2H25GADausnnkFzhZvqi6A35-o_-c')
            ->get();

        $resArr = array();
        if (count($dataArr) > 0) {

            $notification = new Notification();
            $addPush = $notification->add_pushNotification('', $message, $notificationType);

            foreach ($dataArr as $ke => $va) {

                $sendPush = $this->push_notification($va['DeviceToken'], $message, $notificationType, $va['DeviceType']);

                $addSentPush = $notification->add_sentPushLog($va['UserId'], $message, $addPush, $notificationType,'');

            }

        }

        flash()->success('Send Push Successfully');
        return Redirect::to('push-all');
    }

    public function Create()
    {
        return view('admin.Notification.add');
    }

    public function Add(Request $request)
    {
        $notification                   = new Notification();
        $notification->Latitude         = $request->Latitude;
        $notification->Longitude        = $request->Longitude;
        $notification->Range            = $request->Range;
        $notification->NotificationText = $request->NotificationText;
        $notification->NotificationType = '2';
        $result                         = $notification->save();

        if ($result > 0) {
            flash()->success('Push By Location Inserted Successfully');
            return Redirect::to('push-location');
        } else {
            flash()->warning('Error on Create push by location');
            return Redirect::back();
        }

    }

    public function Edit($id)
    {
        $NotificationData = Notification::find($id);
        return view('admin.Notification.edit', compact('NotificationData'));
    }

    public function Update(Request $request)
    {

        $id                             = $request->id;
        $notification                   = Notification::find($id);
        $notification->NotificationText = $request->NotificationText;
        $notification->Latitude         = $request->Latitude;
        $notification->Longitude        = $request->Longitude;
        $notification->Range            = $request->Range;

        $result = $notification->save();

        flash()->success('Push By Location Updated Successfully');
        return Redirect::to('push-location');

    }

    public function Destroy($id)
    {

        Notification::where('NotificationId', $id)->delete();
        echo 'success';
        exit();
    }

    public function PostExpirationReminder()
    {

        // $StartDateTime = date("Y-m-d H:i:s", strtotime('+24 hours'));
        // $EndDateTime   = date("Y-m-d H:i:s", strtotime('+24 hours', strtotime($StartDateTime)));
        // $data          = Vehicle::select('pu.*','tbl_vehicles.VehicleId')
        //                     ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
        //                     ->join('tbl_users as u', 'u.UserId', '=', 'pu.UserId')
        //                     ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'pu.UserId')
        //                     ->where('ns.PostExpirationReminder', 1)
        //                     ->where('VehicleExpiryDate', '>=', (string) $StartDateTime)
        //                     ->where('VehicleExpiryDate', '<=', (string) $EndDateTime)
        //                     ->where('u.Status', '<>', '0')
        //                     // ->where('UserType', '=', '1')
        //                     ->where(function($q) {
        //                         $q->where('UserType','1')
        //                             ->orWhere('UserType', '3');
        //                     })
        //                     ->get();

        $EstimatedExpirationDateTime = date("Y-m-d H:i", strtotime('+24 hours'));
        $data          = Vehicle::select('pu.*','tbl_vehicles.VehicleId','tbl_vehicles.IsOffer')
                            ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
                            ->join('tbl_users as u', 'u.UserId', '=', 'pu.UserId')
                            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'pu.UserId')
                            ->where('ns.PostExpirationReminder', 1)
                            ->where(DB::raw("(DATE_FORMAT(tbl_vehicles.VehicleExpiryDate,'%Y-%m-%d %H:%i'))"),$EstimatedExpirationDateTime)
                            ->where('u.Status', '<>', '0')
                            ->where(function($q) {
                                $q->where('UserType','1')
                                    ->orWhere('UserType', '3');
                            })
                            ->get();

        // $Message = "Your post is going to be exprire soon. please update.";
        $Message = "Tu publicación será exprire pronto. por favor actualice.";
        $notificationType = '5';

        $notification = new Notification();
        $addPush = $notification->add_pushNotification('', $Message, $notificationType);

        foreach ($data as $value) {

            $data = SentPushLog::select()
                    ->where('VehicleId', '=', $value['VehicleId'])
                    ->where('UserId', '=', $value['UserId'])
                    ->whereDate('CreatedAt',date('Y-m-d'))
                    ->where('NotificationType', '=', 5)
                    ->get();

            if(count($data)>0){
                continue;
            }

            if($value['IsOffer']==1){
                // $Message = "Your vehicle is in Offers of the week and your post is going to expire soon.Please update.";
                $Message = "Su vehículo está en las Ofertas de la semana y su publicación caducará pronto. Actualícela.";
            }

            $this->push_notification($value->DeviceToken, $Message, '5', $value->DeviceType);

            $addSentPush = $notification->add_sentPushLog($value['UserId'], $Message, $addPush, $notificationType,$value['VehicleId']);
        }

    }

    public function SendDealOftheWeekReminderToDealer()
    {

    	$CurrentTime = date('H:i');
        $SetTime = '10:00';

        if (date('D') == 'Thu'  && $CurrentTime==$SetTime) {
            $data = Vehicle::select()
                ->join('tbl_users as u', 'u.UserId', '=', 'tbl_vehicles.UserId')
                ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
                ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'u.UserId')
                ->where('ns.SendDealOftheWeekReminderToDealer', 1)
                ->where('IsOffer', '=', '1')
                ->where('UserType', '=', '3')
                ->groupBy('u.UserId')
                ->get();

            $Message = "Your deal of the week is going to be exprire soon please update.";
            $notificationType = '6';

            $notification = new Notification();
            $addPush = $notification->add_pushNotification('', $Message, $notificationType);

            foreach ($data as $value) {
                
                $this->push_notification($value->DeviceToken, $Message, '6', $value->DeviceType);
                $addSentPush = $notification->add_sentPushLog($value['UserId'], $Message, $addPush, $notificationType,'');
            }
        }
    }

    public function SendDealOftheWeekReminderToUser()
    {
        //DB::table('tbl_cronlog')->insert(['CreatedAt' => date('Y-m-d H:i:s')]);
  
        $CurrentTime = date('H:i');
        $SetTime = '10:00';
        // $SetTime = '04:48';

        if (date('D') == 'Sat' && $CurrentTime==$SetTime) {
            $data = PushUser::select()
                    ->join('tbl_users as u', 'u.UserId', '=', 'tbl_pushuser.UserId')
                    ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'u.UserId')
                    ->where('ns.SendDealOftheWeekReminderToUser', 1)
                    ->where('tbl_pushuser.Status', '=', '1')
                    ->get();

            $Message = "Deal of the week is going to be exprire soon, have a look in offer section.";
            $notificationType = '7';

            $notification = new Notification();
            $addPush = $notification->add_pushNotification('', $Message, $notificationType);

            foreach ($data as $value) {
                
                $sendPush = $this->push_notification($value->DeviceToken, $Message, '7', $value->DeviceType);
                $addSentPush = $notification->add_sentPushLog($value['UserId'], $Message, $addPush, $notificationType,'');
            }

        }
    }

    public function ResetOffers()
    {

        $ResetTime ="00:01";
        $CurrentTime = date('H:i');
        
        if (date('D') == 'Mon') {
            if ($CurrentTime == $ResetTime) {
                $data = Vehicle::where('IsOffer', '=', 1)->update(['IsOffer' => 0]);
            }
        }
    }

    public function CheckPostExpirationDate()
    {

        $CurrentDate = date('Y-m-d H:i');

        // $data = Vehicle::where(DB::raw("(DATE_FORMAT(VehicleExpiryDate,'%Y-%m-%d %H:%i'))"),$CurrentDate)->get();

        Vehicle::where(DB::raw("(DATE_FORMAT(VehicleExpiryDate,'%Y-%m-%d %H:%i'))"),$CurrentDate)
                ->where("IsOffer",1)
                ->update(['IsOffer'=>'0']);


    }

    public function test(){
    	$this->push_notification('eWZ4cypTqfE:APA91bEyCzreQxbGCreoFIFqgsJHV80DpV_eWy_ThfPvjA0BBOa10zrht_yyKvWiCS02RULCNg8-fDOs5OS7xK9SYXULzycTEtofPUOF-UyFJIV8lawAQSNI3_jIthSNg7LlqUmOaR4k', 'Test android push', '1', 'android');

        $this->push_notification('dB-RH4r_owg:APA91bGSbcdGLg2zg_4slEAmlottgy3087KCCJpg7SPa7pl0UTG6AlxWJwdhe6wmMHQ1Ap4_piaUdVqXHvFY3uuWZSREFiHspaDvBznK0oBPTbH7bsT3KPoUVAaYIfLwYAy8AlsAbWK4', 'Test ios push', '1', 'ios');
    }

    

    public function push_notification($DeviceToken, $message, $type, $device_os)
    {
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $DeviceToken = array($DeviceToken);

        $api_key = 'AAAAAdH7P6E:APA91bHC3IISlxHxcBOJDx-VluTkBwx7HMSF-u5nqBkY1tU5cG9RLRQe3497kJsjorsg_rgkjscRakClaTfnWTnWjv9aqG1kR9qpo1PMrYcbsxbQ2znMc8ngsBMybUTnjihaFGZYR24v';

        if ($device_os == "Ios" || $device_os == "ios") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                'priority'         => 10,
                'notification'     => array('title' => 'CURA', 'body' => $message, 'sound' => 'Default'),
                'data'             => array('message' => $message, 'type' => $type),
            );
        }

        if ($device_os == "Android" || $device_os == "android") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                'data'             => array("message" => $message, 'type' => $type,'title' => 'CURA'),
            );
        }

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $api_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        // $this->load->helper('file');
        $log = "---------------------------------------------------------------------------\n\n";
        $log .= "LogAt: " . gmdate('Y-m-d H:i:s') . "\n\n";
        $log .= "Type: " . $type . "\n\n";
        $log .= "Message: " . $message . "\n\n";
        $log .= "Device Os: " . $device_os . "\n\n";
        // $log .="Data: [Order->".$order_id."]--[restaurant_name->".$restaurant_name."]--[pickup_address->".$pickup_address."]--[delivery_address->".$delivery_address."]--[order_request_id->".$order_request_id."]--[customer_name->".$customer_name."]\n\n";
        // $log .="ids: ".json_encode($device_ids)."\n\n";
        $log .= "Result: " . $result . "\n\n";
        $log .= "---------------------------------------------------------------------------\n\n";
        $file_path = "LIVE_PUSH_DEBUG1.txt";
        if (file_exists($file_path)) {
            Storage::put($file_path, $log);
        } else {
            Storage::put($file_path, $log);
        }

        // print_r($result);exit;
        // return $result;
    }
}
