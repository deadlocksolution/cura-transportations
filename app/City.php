<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {
	public $timestamps = false;
	protected $primaryKey = 'CityId';
	protected $table = 'tbl_city';

	public function CountryData()
    {
        return $this->hasOne(Country::class, 'CountryId', 'CountryId');
    }

}
