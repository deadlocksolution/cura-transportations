<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushUser extends Model {
	public $timestamps = false;
	protected $primaryKey = 'PushUserId';
    protected $table = 'tbl_pushuser';
}

