<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $timestamps    = false;
    protected $primaryKey = 'NotificationId';
    protected $table      = 'tbl_notifications';

    public function add_pushNotification($UserId,$message,$notificationType){
    	
    	$notification = new Notification();
        $notification->UserId = $UserId;
        $notification->NotificationText = $message;
        $notification->NotificationType = $notificationType;
        $notification->save();

        return $notification->NotificationId;
    }

    public function add_sentPushLog($UserId,$message,$notificationId,$notificationType,$vehicleId){

        $sentPushLog                          = new SentPushLog;
        $sentPushLog->UserId                  = $UserId;
        $sentPushLog->vehicleId               = (isset($vehicleId) && $vehicleId != "") ? $vehicleId : 0;
        $sentPushLog->NotificationId          = $notificationId;
        $sentPushLog->Message                 = $message;
        $sentPushLog->NotificationType        = $notificationType;
        $sentPushLog->CreatedAt               = date('Y-m-d H:i:s');
        $sentPushLog->UpdatedAt               = date('Y-m-d H:i:s');
        $sentPushLog->save();
    }

    function push_notification($DeviceToken,$message,$type,$device_os){
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $DeviceToken = array($DeviceToken);
    
        /*api_key available in:
        Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/   

        $api_key = 'AAAAAdH7P6E:APA91bHC3IISlxHxcBOJDx-VluTkBwx7HMSF-u5nqBkY1tU5cG9RLRQe3497kJsjorsg_rgkjscRakClaTfnWTnWjv9aqG1kR9qpo1PMrYcbsxbQ2znMc8ngsBMybUTnjihaFGZYR24v';

        
        if($device_os == "Ios" || $device_os == "ios"){
            //echo "Ios";
            $fields = array(
                'registration_ids' => $DeviceToken,
                'priority' => 10,
                'notification' => array('title' => 'CURA', 'body' =>  $message ,'sound'=>'Default'),
                // 'notification' => array('title' => 'CURA', 'body' =>  $message ,'sound'=>'Default','message' => $message, 'type' => $type)
            // );),
                'data' => array('message' => $message, 'type' => $type)
            );
        }
        
        if($device_os == "Android" || $device_os == "android"){
             //echo "Android";
            $fields = array (
                'registration_ids' => $DeviceToken,
                'data' => array ("message" => $message,'type' => $type,'title' => 'CURA',)
            );
        }
        
        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$api_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        // $this->load->helper('file');
        $log = "---------------------------------------------------------------------------\n\n";
        $log .="LogAt: ".gmdate('Y-m-d H:i:s')."\n\n";
        $log .="Type: ".$type."\n\n";
        $log .="Message: ".$message."\n\n";
        $log .="Device Os: ".$device_os."\n\n";
        // $log .="Data: [Order->".$order_id."]--[restaurant_name->".$restaurant_name."]--[pickup_address->".$pickup_address."]--[delivery_address->".$delivery_address."]--[order_request_id->".$order_request_id."]--[customer_name->".$customer_name."]\n\n";
        // $log .="ids: ".json_encode($device_ids)."\n\n";
        $log .="Result: ".$result."\n\n";
        $log .= "---------------------------------------------------------------------------\n\n";
        $file_path = "LIVE_PUSH_DEBUG.txt";
        // if(file_exists($file_path))
        // {
        //     Storage::put($file_path, $log);
        // }
        // else
        // {
        //     Storage::put($file_path, $log );
        // }
       
        return $result;
    }

}
