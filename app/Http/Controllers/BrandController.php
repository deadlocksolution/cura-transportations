<?php
namespace App\Http\Controllers;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BrandController extends Controller
{

    public function Index($type)
    {

        $type = ($type == 'auto') ? 1 : 2;

        $dataArr = Brand::select()
            ->where(['VehicleType' => $type])
            ->orderBy("BrandId", "ASC")
            ->get();

        return view('admin.Brand.index', compact('dataArr'));

    }

    public function Create()
    {
        return view('admin.Brand.add');
    }

    public function Store(Request $request)
    {

        $brand                    = new Brand();
        $brand->BrandCode         = $request->code;
        $brand->Name              = $request->name;
        $brand->VehicleType       = $request->vehicletype;
        $brand->Status            = $request->status;
        $brand->VehicleCategories = implode(',', $request->vehiclecategories);
        $brand->save();

        flash()->success('Brand Inserted Successfully');
        if( $request->vehicletype == 1 || $request->vehicletype == '1'){
            return Redirect::to('brand/type/auto');    
        } else {
            return Redirect::to('brand/type/motorcycle');    
        }
        

    }

    public function Edit($id)
    {
        $BrandData = Brand::find($id);
        return view('admin.Brand.edit', compact('BrandData'));
    }

    public function Update(Request $request)
    {

        $id                       = $request->id;
        $brand                    = Brand::find($id);
        $brand->Name              = $request->name;
        $brand->VehicleType       = $request->vehicletype;
        $brand->Status            = $request->status;
        $brand->VehicleCategories = implode(',', $request->vehiclecategories);

        $result = $brand->save();

        flash()->success('Brand Updated Successfully');
        if( $request->vehicletype == 1 || $request->vehicletype == '1'){
            return Redirect::to('brand/type/auto');    
        } else {
            return Redirect::to('brand/type/motorcycle');    
        }

    }

    public function Destroy($id)
    {

        Brand::where('BrandId', $id)->delete();
        echo 'success';
        exit();
    }

}
