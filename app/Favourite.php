<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model {
	public $timestamps = false;
	protected $primaryKey = 'FavouriteId';
    protected $table = 'tbl_favourite';

    public function user() 
	{ 
	    return $this->hasOne(User::class, 'UserId', 'UserId'); 
	}
}
