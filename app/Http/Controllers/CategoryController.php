<?php 
namespace App\Http\Controllers;


use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\VehicleCategory;
use Session;

use Validator;
use Sentinel;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller {


	public function Index($type){
		
		$type = ($type == 'auto') ? 1 : 2;
			
		$dataArr = VehicleCategory::select()
							->where(['VehicleType' => $type])
							->orderBy("VehicleCategoryId", "DESC")
							->get();

		return view('admin.Category.index', compact('dataArr'));

	}

	public function Create(){
        return view('admin.Category.add');
    }

    public function Store(Request $request){

    	 $validator = Validator::make($request->all(), [
                'name' => 'required|unique:tbl_vehiclecategory,VehicleName'
                ]
        );
        if ($validator->fails()) {
            flash()->warning('Category is already exist. please use unique one');
            return Redirect::back()->withInput();
        }
        
        $category = New VehicleCategory();
        $category->VehicleName = $request->name;
        $category->VehicleType = $request->vehicletype;

        

        if ($request->hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = 'uploads/category/';
            $file_name       = "category_" . time() . '.png';
            $file->move($destinationPath, $file_name);
            $profile_pic = 'uploads/category/' . $file_name;
            $category->Image =$profile_pic;
        }
        
        $category->save();
        
        flash()->success('Category Inserted Successfully');
        return Redirect::to('category/type/auto');
        
    }

    public function Edit($id){
        $CategoryData = VehicleCategory::find($id);
        return view('admin.Category.edit', compact('CategoryData'));
    }

    public function Update(Request $request){

        $id = $request->id;

        
        $category = VehicleCategory::find($id);  
        $category->VehicleName = $request->name;
        $category->VehicleName = $request->vehicletype;

        if ($request->hasFile('image')) {
            $file = Input::file('image');
            $destinationPath = 'uploads/category/';
            $file_name       = "category_" . time() . '.png';
            $file->move($destinationPath, $file_name);
            $profile_pic = 'uploads/category/' . $file_name;
            $category->Image =$profile_pic;
        }


        $result = $category->save();

        flash()->success('Product Type Updated Successfully');
        return Redirect::to('category/type/auto');

    }



    public function Destroy($id){

        VehicleCategory::where('VehicleCategoryId', $id)->delete();
        echo 'success';
        exit();
    }

  
}