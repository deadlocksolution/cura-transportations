<html>
    <head>
        <title>CURA App Test Api</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style>
            div{
                float: left;
            }
        </style>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

        <script type="text/javascript">
            !function(){var e='/* Syntax highlighting for JSON objects */ .json-editor-blackbord {   background: #1c2833;   color: #fff;   font-size: 13px;   font-family: Menlo,Monaco,Consolas,"Courier New",monospace; } @media screen and (min-width: 1600px) {   .json-editor-blackbord {     font-size: 14px;   } }  ul.json-dict, ol.json-array {   list-style-type: none;   margin: 0 0 0 1px;   border-left: 1px dotted #525252;   padding-left: 2em; } .json-string {   /*color: #0B7500;*/   /*color: #BCCB86;*/   color: #0ad161; } .json-literal {   /*color: #1A01CC;*/   /*font-weight: bold;*/   color: #ff8c00; } .json-url {   color: #1e90ff; } .json-property {   color: #4fdee5;   line-height: 160%;   font-weight: 500; }  /* Toggle button */ a.json-toggle {   position: relative;   color: inherit;   text-decoration: none;   cursor: pointer; } a.json-toggle:focus {   outline: none; } a.json-toggle:before {   color: #aaa;   content: "\\25BC"; /* down arrow */   position: absolute;   display: inline-block;   width: 1em;   left: -1em; } a.json-toggle.collapsed:before {   transform: rotate(-90deg); /* Use rotated down arrow, prevents right arrow appearing smaller than down arrow in some browsers */   -ms-transform: rotate(-90deg);   -webkit-transform: rotate(-90deg); }   /* Collapsable placeholder links */ a.json-placeholder {   color: #aaa;   padding: 0 1em;   text-decoration: none;   cursor: pointer; } a.json-placeholder:hover {   text-decoration: underline; }',o=function(e){var o=document.getElementsByTagName("head")[0],t=document.createElement("style");if(o.appendChild(t),t.styleSheet)t.styleSheet.disabled||(t.styleSheet.cssText=e);else try{t.innerHTML=e}catch(n){t.innerText=e}};o(e)}(),function(e){function o(e){return e instanceof Object&&Object.keys(e).length>0}function t(e){var o=/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;return o.test(e)}function n(e,r){var s="";if("string"==typeof e)e=e.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;"),s+=t(e)?'<a href="'+e+'" class="json-string json-url">"'+e+'"</a>':'<span class="json-string">"'+e+'"</span>';else if("number"==typeof e)s+='<span class="json-literal json-literal-number">'+e+"</span>";else if("boolean"==typeof e)s+='<span class="json-literal json-literal-boolean">'+e+"</span>";else if(null===e)s+='<span class="json-literal json-literal-null">null</span>';else if(e instanceof Array)if(e.length>0){s+='[<ol class="json-array">';for(var l=0;l<e.length;++l)s+="<li>",o(e[l])&&(s+='<a href class="json-toggle"></a>'),s+=n(e[l],r),l<e.length-1&&(s+=","),s+="</li>";s+="</ol>]"}else s+="[]";else if("object"==typeof e){var a=Object.keys(e).length;if(a>0){s+='{<ul class="json-dict">';for(var i in e)if(e.hasOwnProperty(i)){s+="<li>";var c=r.withQuotes?'<span class="json-string json-property">"'+i+'"</span>':'<span class="json-property">'+i+"</span>";s+=o(e[i])?'<a href class="json-toggle"></a>'+c:c,s+=": "+n(e[i],r),--a>0&&(s+=","),s+="</li>"}s+="</ul>}"}else s+="{}"}return s}e.fn.jsonViewer=function(t,r){return r=r||{},this.each(function(){var s=n(t,r);o(t)&&(s='<a href class="json-toggle"></a>'+s),e(this).html(s),e(this).off("click"),e(this).on("click","a.json-toggle",function(){var o=e(this).toggleClass("collapsed").siblings("ul.json-dict, ol.json-array");if(o.toggle(),o.is(":visible"))o.siblings(".json-placeholder").remove();else{var t=o.children("li").length,n=t+(t>1?" items":" item");o.after('<a href class="json-placeholder">'+n+"</a>")}return!1}),e(this).on("click","a.json-placeholder",function(){return e(this).siblings("a.json-toggle").click(),!1}),1==r.collapsed&&e(this).find("a.json-toggle").click()})}}(jQuery),function(e){function o(e){var o={'"':'\\"',"\\":"\\\\","\b":"\\b","\f":"\\f","\n":"\\n","\r":"\\r","    ":"\\t"};return e.replace(/["\\\b\f\n\r\t]/g,function(e){return o[e]})}function t(e){if("string"==typeof e)return o(e);if("object"==typeof e)for(var n in e)e[n]=t(e[n]);else if(Array.isArray(e))for(var r=0;r<e.length;r++)e[r]=t(e[r]);return e}function n(o,t,n){n=n||{},n.editable!==!1&&(n.editable=!0),this.$container=e(o),this.options=n,this.load(t)}n.prototype={constructor:n,load:function(e){this.$container.jsonViewer(t(e),{collapsed:this.options.defaultCollapsed,withQuotes:!0}).addClass("json-editor-blackbord").attr("contenteditable",!!this.options.editable)},get:function(){try{return this.$container.find(".collapsed").click(),JSON.parse(this.$container.text())}catch(e){throw new Error(e)}}},window.JsonEditor=n}(jQuery);

        </script>
        
        
        <script>
            window.api_id = '87cc4db7f5047011292b841943f95ed5';
            window.api_secret = '67c0786009b0eec4a0283c6c78969863';
            var testcases = [];

            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: '--- GENERAL SERVICES ---',
                data: {
                }
            });

            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: 'ForcefullyUpdateApp',
                data: {
                    'AppVersion': 'v1',
                    'DeviceType': 'ios',
                }
            });

            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: 'GenerateOtp',
                data: {
                    'ContactNumber': '9537965166',
                    'CountryCode': '+91',
                }
            });


            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: 'SignUp',
                data: {
                    'FirstName':'Test',
                    'LastName':'User',
                    'CountryCode':'+91',
                    'ContactNumber':'123456789',
                    'Email': 'iroid.test1@gmail.com',
                    'ProfilePic':'image data',
                    'Latitude':'21.1901',
                    'Longitude':'72.8139',
                    'City':'Surat'
                    
                }
            });


            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: 'ChangeContactNumber',
                data: {
                    'UserId':'1',
                    'CountryCode':'+91',
                    'ContactNumber':'123456789',
                    'Type':'1: Request Otp, 2: Update ContactNumber'
                    
                }
            });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'SocialLogin',
            //     data: {
            //         'SocialId': "741258963214",
            //         'SocialType': "Facebook/Google",
            //         'FullName':'Test',
            //         'Email': 'iroid.test1@gmail.com',
            //         'ContactNumber': '0000000000',
            //         'DateOfBirth' : '2010-01-01',   
            //         'Gender' : '0:Not Set,1:Male,2:Female,3:Others',
            //         'ProfilePic':'image data'
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'Login',
            //     data: {
            //         'Email': 'iroid.test1@gmail.com',
            //         'Password': '098f6bcd4621d373cade4e832627b4f6',
            //         // 'Type': '1 : User, 3 : Store User',
            //         'Secret': '7629660670a3ad7613e56f048e217c10'     
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'GuestLogin',
            //     data: {
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'ForgotPassword',
            //     data: {      
            //         'Email': 'iroid.test1@gmail.com'                    
            //     }
            // });

            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: 'UpdateProfile',
                data: {
                    'UserId': '1',
                    'FirstName':'Test',
                    'LastName':'User',
                    'Email': 'iroid.test1@gmail.com',
                    'DateOfBirth' : '01-01-1990',   
                    'Gender' : '0:Not Set,1:Male,2:Female,3:Others',
                    'Latitude':'21.1901',
                    'Longitude':'72.8139',
                    'City':'Surat',
                    'ProfilePic':'image data'
                }
            });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'ChangePassword',
            //     data: {
            //         'UserId':'1',
            //         'CurrentPassword':'098f6bcd4621d373cade4e832627b4f6',
            //         'NewPassword':'5a105e8b9d40e1329780d62ea2265d8a'
            //     }
            // });


            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'RegisterForPush',
            //     data: {
            //         'UserId': '1',
            //         'DeviceId': '123456789',
            //         'DeviceType': 'ios',
            //         'DeviceToken': '212345364',
            //         'CertificateType' : '0 where 0:dev, 1:live '
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'UpdateLatLong',
            //     data: {
            //         'UserId': '1',
            //         'UserLatitude':'21.1901',
            //         'UserLongitude':'72.8139'
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'Logout',
            //     data: {
            //         'UserId': '1',
            //         'DeviceType': 'ios',
            //         'DeviceId': 'Your Device Id',
            //         'DeviceToken': 'Your Device Token',
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'GetUserDetails',
            //     data: {
            //         'UserId': '1'
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'updatePassword',
            //     data: {
            //         'UserId':'1',
            //         'OldPassword':'098f6bcd4621d373cade4e832627b4f6',
            //         'NewPassword':'5a105e8b9d40e1329780d62ea2265d8a'
            //     }
            // });

            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'CheckEmail',
            //     data: {
            //         'Email': 'iroid.test1@gmail.com',
            //     }
            // });

            testcases.push({
                api_id: window.api_id,
                api_secret: window.api_secret,
                api_request: '--- VEHICLE SERVICES ---',
                data: {
                }
            });

        
            // testcases.push({
            //     api_id: window.api_id,
            //     api_secret: window.api_secret,
            //     api_request: 'GetTermsCondition',
            //     data: {
            //     }
            // });


            // var editor = new JsonEditor('#json-display', getJson());
            // JSON editor

            var responseEditor;
            var requestEditor;

            function runTestcase() {
                responseEditor = new JsonEditor('#json-response-display', getResponseJson());
                // var data;
                try {
                    // var data = JSON.parse($('.testcase').val());
                    var data = requestEditor.get();
                    console.log(data);
                } catch (e) {
                    $('.output').val('Invalid JSON.');
                    return;
                }
                if (data) {
                    var Authorization = $('.Authorization').val();
                    var DeviceType = $('.DeviceType').val();
                    var DeviceId = $('.DeviceId').val();
                    var TestMode = $('.TestMode').val();
                    var req= JSON.parse($('.testcase').val());
                    if((req.api_request != 'GenerateOtp'  && req.api_request != 'GuestLogin'  && req.api_request != 'SignUp' && req.api_request != 'ForgotPassword' && req.api_request != 'signUp_test' && req.api_request != 'ForgotPassword' && req.api_request != 'changePassword' && req.api_request != 'checkPromocode' && req.api_request != 'getPlan' && req.api_request != 'logout' && req.api_request != 'ForcefullyUpdateApp' && req.api_request != 'checkEmailAvailability' && req.api_request != 'verifyLink' && req.api_request != 'GetTermsCondition') || TestMode != "1" ){
                        if(Authorization == ""){
                            swal('Api','Authorization can not be empty','warning');
                            return false;
                        }
                        if(DeviceType == ""){
                            swal('Api','Device type can not be empty.','warning');
                            return false;
                        }
                        if(DeviceId == ""){
                            swal('Api','Device id can not be empty.','warning');
                            return false;
                        }
                    }
            
                    $.ajax({
                        method: 'POST',
                        url: 'api',
                        headers: { 'Authorization': Authorization, 'Devicetype': DeviceType, 'Deviceid': DeviceId, 'TestMode': TestMode,  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // data: JSON.parse($('.testcase').val()),
                        data: requestEditor.get(),
                        
                        success: function (responsejson) {
                            // console.log("result : "+responsejson);die();
                            if (typeof responsejson == 'string' || responsejson instanceof String) {
                                try {
                                    var output = JSON.parse(responsejson);
                                    $('.output').val(JSON.stringify(output, null, 4));
                                    responseEditor.load(getResponseJson());
                                    if(req.api_request == 'Login' || req.api_request == 'GenerateOtp'){
                                        var AccessToken = 'Elephant'+' '+output['data']['AccessToken'];
                                        //$('#Authorization').val(AccessToken);
                                        localStorage.setItem("AccessToken", AccessToken);
                                    }    
                                } catch (e) {
                                    $('.output').val(responsejson);
                                    responseEditor.load(getResponseJson());

                                }
                            } else {
                                $('.output').val(JSON.stringify(responsejson, null, 4));
                                responseEditor.load(getResponseJson());
                            }
                            if ($('.output').val() == '') {
                                $('.output').val('No output.');
                                responseEditor.load(getResponseJson());
                            }
                        },
                        error: function (data, status, error_thrown) {
                            $('.output').val('Error: ' + error_thrown);
                        }
                    });
                }
            }
            function encrypt() {
                window.location = '?input=' + $('.testcase').val() + '&encrypt=1';
            }
            function decrypt() {
                window.location = '?input=' + $('.testcase').val() + '&decrypt=1';
            }
            function toJSON(responsejson) {
                try {
                    var response = JSON.parse(responsejson);
                    return response;
                } catch (e) {
                    return responsejson;
                }
            }
            $(document).ready(function () {

                
                requestEditor = new JsonEditor('#json-request-display', getRequestJson());
                responseEditor = new JsonEditor('#json-response-display', getResponseJson());

                for (var i = 0; i < testcases.length; i++) {
                    $('.select').append('<option value="' + i + '">' + testcases[i].api_request + '</option>');
                }
                $('.select').change(function () {
                    


                    var selected_service = $('.select option:selected').text();
                    if(selected_service == "Login" || selected_service == "SignUp" || selected_service == "ForgotPassword" || selected_service == "GenerateOtp"){
                      $('#Authorization').val('');  
                    }else{
                        var AccessToken =  localStorage.getItem("AccessToken");
                        if(AccessToken !=""){
                            $('#Authorization').val(AccessToken);
                        }
                    }

                    if ($('.select').val() != -1) {

                      
                        $('.testcase').val(JSON.stringify(testcases[$('.select').val()], null, 4));

                        requestEditor.load(getRequestJson());
          

                    }
                });
            });

            $(function() {
              $(".select").select2();
            }); 

            function getRequestJson() {
              try {
                return JSON.parse($('.testcase').val());
              } catch (ex) {
                // alert('Wrong JSON Format: ' + ex);
              }
            }

            function getResponseJson() {
              try {
                return JSON.parse($('.output').val());
              } catch (ex) {
                // alert('Wrong JSON Format: ' + ex);
              }
            }


        </script>
        <style>
            input{
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
            }
            .headers{
                padding: 10px;
                font-weight: 900;
            }
            ::-webkit-scrollbar {
                width: 5px;
                
            }
             
            ::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
                border-radius: 10px;
            }
             
            ::-webkit-scrollbar-thumb {
                border-radius: 10px;
                -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
                    background: #cdcdcd;
            }
            textarea {
                font-family: monospace;
                width: 100%;
                min-height: 600px;
                border-radius: 5px;
            }
            .select2-container--default .select2-results>.select2-results__options{
                max-height: 595 !important;
            }
            pre.form-control {
                font-family: monospace;
                width: 100%;
                min-height: 500px;
                border-radius: 5px;
                font-size: 16px;
                max-height: 500px;
            }
            pre.form-control:focus{
                     background-color: #1c2833; 
            }
           
        </style>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    </head>
    <body>
        <div class="col-md-12" style="margin-top:10px;">
            <div class="col-md-12" style="margin-bottom:10px;">
                <select size="1" class="select" style="width: 600px; height: 40px; font-size: 16px;border-radius: 5px;">
                    <option value="-1">-SELECT TESTCASE-</option>
                </select><button class="btn btn-primary" onclick="runTestcase();" style="margin-left:10px; ">Run</button>
            </div>
            <div class="col-md-6" style="margin-bottom:10px;margin-left:10px;padding: 0px;">
                 <div class="col-md-2 headers">Authorization</div><input id="Authorization" class="Authorization col-md-7" type="text" name="Authorization" placeholder="[ AccessKey ]  [ AccessToken ]" value="" style="margin: 5px;">
            </div>   
            <div class="col-md-6" style="margin-bottom:10px;margin-left:10px;padding: 0px;">
                
                 <div class="col-md-2 headers">Device type</div><input class="DeviceType col-md-7" type="text" name="DeviceType" placeholder="Device type" value="ios" style="margin: 5px;">
            </div>   
            <div class="col-md-6" style="margin-bottom:10px;margin-left:10px;padding: 0px;">
                 <div class="col-md-2 headers">Device id</div><input class="DeviceId col-md-7" type="text" name="DeviceId" placeholder="Device id" value="123456" style="margin: 5px;">
            </div>
            <div class="col-md-6" style="margin-bottom:10px;margin-left:10px;padding: 0px;display: none;">
                 <div class="col-md-2 headers">TestMode</div><input class="TestMode col-md-7" type="hidden" name="TestMode" placeholder="" value="1" style="margin: 5px;">
            </div>
            <div class="col-md-6 notes" style="margin-bottom:10px;margin-left:10px;padding: 0px;display: none;">
                 <div class="col-md-12" style="padding: 10px;">   
                    <span><b>Note : </b> <span class="note_text"></span></span>
                 </div>   
            </div>
            <div class="col-md-6">
                <span style="font-weight: 900;">API Request:</span>
                <br>   
                <pre id="json-request-display" class="form-control" contenteditable="true"></pre>
                <textarea class="testcase" style="display: none;"></textarea>
            </div>
            <div class="col-md-6">
                <span style="font-weight: 900;">Response:</span>
                <br> 
                <pre id="json-response-display"  class="form-control" contenteditable="true"></pre>
                <textarea class="output" style="display: none;"></textarea>
            </div>
        </div>
    </body>
    
</html>