@extends('layouts.admin_template')
@section('title', 'Update Category') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Update Category</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('category.index') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['route'=>['category.update',$categoryData->id], 'enctype' => 'multipart/form-data', 'method'=>'put'])}} 
                                        <div class="form-body">
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="name">Category Name *</label>
                                                        <input type="text" id="name" name="name" class="form-control" required="" placeholder="Enter Name" value="{{ $categoryData->name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="logo">Category Logo</label>
                                                        <input type="file" id="logo" name="logo">
                                                        
                                                        <br/><br/>
                                                        
                                                        @if($categoryData->logo != "")
                                                        <img src="{{ asset("public/images/category") . "/" . $categoryData->logo }}" width="100px" height="100px">
                                                        @endif
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="description">Description</label>
                                                        <textarea id="description" name="description" class="form-control" placeholder="Enter Description">{{ $categoryData->description }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection
