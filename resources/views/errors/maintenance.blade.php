<!DOCTYPE html>
<html>
    <head>
        <title>Website under maintenance</title>
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/bootstrap.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/bootstrap-theme.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/style.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/owl.carousel.min.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/owl.theme.default.min.css") }}">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/css/nice-select.css") }}">
        <link rel="stylesheet" type="text/css" href="{{ asset("public/site/fonts/font-awesome-4.7.0/css/font-awesome.min.css") }}">
    </head>
    <body class="under-construction">

        <!-- main -->
        <div class="under-construction">
        
        </div>  
        <!-- //main --> 
        
        <script type="text/javascript" src="{{ asset("public/site/js/jquery.slim.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("public/site/js/bootstrap.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("public/site/js/moment.js") }}"></script>
    </body>
</html> 