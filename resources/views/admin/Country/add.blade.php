@extends('layouts.admin_template')
@section('title', 'Add City') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Add City</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('country') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('city.store'), 'role'=>'form']) }}
                                        <div class="form-body">
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="CountryId">SELECT COUNTRY</label>
                                                        <select class="form-control" name="CountryId" id="CountryId">
                                                            @if(!empty($countryArr))
                                                                <option value=""> SELECT COUNTRY  </option>
                                                                @foreach($countryArr as $key => $value)
                                                                    <option value="{{ $value->CountryId }}"> {{ $value->CountryName }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="CityName">CITY NAME</label>
                                                        <input type="text" id="CityName" name="CityName" class="form-control" required="" placeholder="" value="{{ old('CityName') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Currency">ENTER CURRENCY NAME</label>
                                                        <input type="text" id="phonecode" name="Currency" class="form-control" required="" placeholder="" value="{{ old('Currency') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Latitude">LATITUDE</label>
                                                        <input type="text" id="phonecode" name="Latitude" class="form-control" required="" placeholder="eg:23.3432" onkeypress="return isNumber(event)" value="{{ old('Latitude') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Longitude">LONGITUDE</label>
                                                        <input type="text" id="Longitude" name="Longitude" class="form-control" required="" placeholder="eg:23.3432" onkeypress="return isNumber(event)" value="{{ old('Longitude') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        function getCity(countryId){
            console.log("country : "+countryId);
            exit();

            var categoryId = $('#vehiclecategory').val();

            $.ajax({    
                type: "GET",
                url: "../vehicle/get_brands/"+type+"/"+categoryId,
                data: {
                    // 'type' : type
                },
                success: function (res) {
                    var obj = JSON.parse(res);
                    $('#brands').prop('disabled', false).html('').append('<option value="">Seleccione uno </option>');
                    for (var i = 0; i < obj.length; i++) {
                        $('#brands').append('<option value=' + obj[i].BrandId + '>' + obj[i].Name + '</option>');
                    }
                }
            });

        }

    </script>
@endpush

