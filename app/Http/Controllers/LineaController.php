<?php
namespace App\Http\Controllers;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Line;
use App\VehicleCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LineaController extends Controller
{

    public function Index($type)
    {

        $type = ($type == 'auto') ? 1 : 2;

        $dataArr = Line::with('brand_data', 'category_data')
            ->where(['VehicleType' => $type])
            ->orderBy("LineId", "ASC")
            ->get();

        return view('admin.Linea.index', compact('dataArr'));

    }

    public function Create()
    {
        return view('admin.Linea.add');
    }

    public function Store(Request $request)
    {

        $linea                    = new Line();
        $linea->LineCode          = $request->code;
        $linea->Name              = $request->name;
        $linea->VehicleType       = $request->vehicletype;
        $linea->VehicleCategoryId = implode(',',$request->category);
        $linea->BrandId           = $request->brand;
        $linea->Status            = $request->status;
        $linea->save();

        flash()->success('Linea Inserted Successfully');
        if( $request->vehicletype == 1 || $request->vehicletype == '1'){
            return Redirect::to('linea/type/auto');
        } else {
            return Redirect::to('linea/type/motorcycle');    
        }
        

    }

    public function Edit($id)
    {

        $LineaData = Line::find($id);

        return view('admin.Linea.edit', compact('LineaData'));
    }

    public function Update(Request $request)
    {

        $id                       = $request->id;
        $linea                    = Line::find($id);
        $linea->Name              = $request->name;
        $linea->VehicleType       = $request->vehicletype;
        $linea->VehicleCategoryId = implode(',',$request->category);
        $linea->BrandId           = $request->brand;
        $linea->Status            = $request->status;

        $result = $linea->save();

        flash()->success('Linea Updated Successfully');
        if( $request->vehicletype == 1 || $request->vehicletype == '1'){
            return Redirect::to('linea/type/auto');
        } else {
            return Redirect::to('linea/type/motorcycle');    
        }

    }

    public function GetCategory(Request $request)
    {

        $categoryArr      = VehicleCategory::where(['VehicleType' => $request->type])->get();
        $brandArr         = Brand::where(['VehicleType' => $request->type])->get();
        $data             = array();
        $data['category'] = $categoryArr;
        $data['brand']    = $brandArr;
        echo json_encode($data);
        die();
    }

    public function Destroy($id)
    {

        Line::where('LineId', $id)->delete();
        echo 'success';
        exit();
    }

}
