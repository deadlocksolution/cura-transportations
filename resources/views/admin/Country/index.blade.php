@extends('layouts.admin_template')
@section('title', 'Country List') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Country List</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6"><a href="#" class="btn btn-primary pull-right m-2" data-toggle="modal" data-target="#CountryModal">Add Country</a></div>
            <div class="col-lg-2 col-sm-6 col-md-6 col-xs-6"><a href="{{ route('city.create') }}" class="btn btn-primary pull-right m-2">Add City</a></div>
            <div class="col-lg-4 col-sm-6 col-md-6 col-xs-6"><a href="#" class="btn btn-primary pull-right m-2"
                data-toggle="modal" data-target="#ActivateCityModal" >Configure & Activate City</a></div>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>COUNTRY</th>
                                            <th>CITY</th>
                                            <th>LATITUDE</th>
                                            <th>LONGITUDE</th>
                                        </tr>
                                    </thead>
                            
                                    <tbody>
                                        
                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->CityId }}" id="record_{{ $data_value->CityId }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    <td>{{ $data_value->CountryData->CountryName }}</td>
                                                    <td>{{ $data_value->CityName }}</td>
                                                    <td>{{ $data_value->Latitude }}</td>
                                                    <td>{{ $data_value->Longitude }}</td>
                                                    
                                                    <td>
                                                        <a href="{{ route('country.edit', array($data_value->id)) }}">
                                                            <i class="fa fa-pencil-square-o fa-2x"></i> 
                                                        </a>&nbsp;&nbsp;   
                                                    
                                                        <a href="javascript:void(0);" onclick="deletePopup('country/delete/', {{ $data_value->id }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>


<!-- COUNTRY CONTROLLER -->
<div id="CountryModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Country</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => route('country.store'), 'role'=>'form']) }}
                    <div class="form-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="CountryName">ENTER COUNTRY NAME *</label>
                                    <input type="text" id="CountryName" name="CountryName" class="form-control" required="" placeholder="Enter Name" value="{{ old('CountryName') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="form-actions mt-10">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
              <!--   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>


<!-- ACTIVE CITY CONTROLLER -->
<div id="ActivateCityModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select City and Country</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => route('country.activate_city'), 'role'=>'form']) }}
                    <div class="form-body">
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="CountryId">SELECT COUNTRY</label>
                                    <select class="form-control" name="CountryId" id="CountryId" onchange="getCity(this.value)">
                                    @if(!empty($countryArr))
                                        <option value=""> SELECT COUNTRY  </option>
                                        @foreach($countryArr as $key => $value)
                                            <option value="{{ $value->CountryId }}"> {{ $value->CountryName }}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="CityId">SELECT CITY</label>
                                    <select class="form-control" name="CityId" id="CityId"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="form-actions mt-10">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
              <!--   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 6 ] }],
                "aaSorting": [[5,'DESC']]
            });  
        });  
    </script>

    <script type="text/javascript">
        function getCity(countryId){
 
            $.ajax({    
                type: "GET",
                url: "country/get_city/"+countryId,
                success: function (res) {

                    var obj = JSON.parse(res);
                    $('#CityId').html('');

                    if(obj.length > 0){
                        for (var i = 0; i < obj.length; i++) {
                            $('#CityId').append('<option value=' + obj[i].CityId + '>' + obj[i].CityName + '</option>');
                        }
                    }
                    
                }
            });

        }

    </script>
@endpush

