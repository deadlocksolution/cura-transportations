@extends('layouts.admin_template')
@section('title', 'Update Location') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Update Location</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('location.index') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default card-view">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">

                                            @if (session()->has('flash_notification.message'))
                                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    {!! session('flash_notification.message') !!}
                                                </div>
                                            @endif

                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="form-wrap">
                                                        {{ Form::open(['route'=>['location.update',$locationData->id], 'enctype' => 'multipart/form-data', 'method'=>'put'])}} 
                                                        
                                                            <div class="form-body">

                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        @if($user_type == "admin")
                                                                            <div class="form-group">
                                                                                <label class="control-label mb-10" for="user_id">Manufacturer *</label>
                                                                                <select id="user_id" name="user_id" class="form-control select2">
                                                                                    <option value="">Select Manufacturer</option>
                                                                                    @foreach($manufacturerData AS $manufacturer_value)
                                                                                    <option value="{{ $manufacturer_value->id }}" <?php echo (isset($locationData->user_id) && $locationData->user_id == $manufacturer_value->id) ? 'selected="selected"' : ""; ?>>{{ $manufacturer_value->first_name }} {{ $manufacturer_value->last_name }} ({{ $manufacturer_value->email }})</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        @else
                                                                            <input type="hidden" name="user_id" value="{{ $user_id }}" >
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="address_type_id">Address Type *</label>
                                                                            <select id="address_type_id" name="address_type_id" class="form-control select2">
                                                                                <option value="">Select Address Type</option>
                                                                                @foreach($address_type_data AS $address_type_value)
                                                                                <option value="{{ $address_type_value->id }}" <?php echo (isset($locationData->address_type_id) && $address_type_value->id == $locationData->address_type_id) ? 'selected="selected"' : ""; ?>>{{ $address_type_value->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="country">Country *</label>
                                                                            <select class="form-control select2" required="" name="country_id" id="country" onchange="changeCountry(this.value);">
                                                                                <option value="">Select Country</option>
                                                                                @foreach($country_data AS $country_value)
                                                                                <option value="{{ $country_value['id'] }}" <?php echo (isset($locationData->country_id) && $country_value['id'] == $locationData->country_id) ? 'selected="selected"' : ""; ?>>{{ $country_value['name'] }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="state">State *</label>
                                                                            <select class="form-control select2" required="" name="state_id" id="state" onchange="changeState(this.value);">
                                                                                <option value="">Select State</option>
                                                                                @foreach($state_data AS $state_value)
                                                                                <option value="{{ $state_value['id'] }}" <?php echo (isset($locationData->state_id) && $locationData->state_id == $state_value['id']) ? 'selected="selected"' : ""; ?>>{{ $state_value['name'] }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="city">City *</label>
                                                                            <select class="form-control select2" required="" name="city_id" id="city">
                                                                                <option value="">Select City</option>
                                                                                @foreach($city_data AS $city_value)
                                                                                <option value="{{ $city_value['id'] }}" <?php echo (isset($locationData->city_id) && $locationData->city_id == $city_value['id']) ? 'selected="selected"' : ""; ?>>{{ $city_value['name'] }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="control-label mb-10" for="status">Status</label>
                                                                            <select class="form-control" name="status" id="status">
                                                                                <option value="1" <?php echo (isset($locationData->status) && $locationData->status == 1) ? 'selected="selected"' : ""; ?>>Enable</option>
                                                                                <option value="0" <?php echo (isset($locationData->status) && $locationData->status == 0) ? 'selected="selected"' : ""; ?>>Disable</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-md-4">
                                                                        <div class="form-group" id="address_div">
                                                                            <label class="control-label mb-10">Address</label>
                                                                            
                                                                            @if(isset($locationData->location_data) && $locationData->location_data->count() > 0)
                                                                                @php $location_cnt = 1; @endphp
                                                                                @foreach($locationData->location_data AS $location_value)
                                                                                    
                                                                                    <div class="row" id="address_box_{{ $location_cnt }}">
                                                                                        <div class="col-md-12 row">
                                                                                            <div class="col-md-11">
                                                                                                <input type="text" class="form-control mb-20" name="address[]" value="{{ $location_value->address }}">
                                                                                            </div>
                                                                                            <div class="col-md-1">
                                                                                                @if($location_cnt == 1)
                                                                                                    <i class="fa fa-plus color-green fa-2x cursor-pointer" onclick="addMoreAddress();"></i>
                                                                                                @else
                                                                                                    <i class="fa fa-minus color-red fa-2x cursor-pointer" onclick="removeAddress({{ $location_cnt }});"></i>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                
                                                                                    @php $location_cnt++; @endphp
                                                                                @endforeach
                                                                            @else
                                                                                <div class="row" id="address_box_1">
                                                                                    <div class="col-md-12 row">
                                                                                        <div class="col-md-11">
                                                                                            <input type="text" class="form-control mb-20" name="address[]">
                                                                                        </div>
                                                                                        <div class="col-md-1">
                                                                                            <i class="fa fa-plus color-green fa-2x cursor-pointer" onclick="addMoreAddress();"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                        <input type="hidden" id="cnt" value="<?php echo (isset($locationData->location_data) && $locationData->location_data->count() > 0) ? $locationData->location_data->count() : 1; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-actions mt-10">
                                                                <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                                            </div>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
@endpush