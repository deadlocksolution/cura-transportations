@extends('layouts.admin_template')
@section('title', 'Sub Category List') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Sub Category List</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            @if($is_admin == 0)
            <a href="{{ route('subcategory.create') }}" class="btn btn-primary pull-right">Add Sub Category</a>
            @endif
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            @if($is_admin == 1)
                                            <th>Added By</th>
                                            @endif
                                            <th>Main Category Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            @if($is_admin == 1)
                                            <th>Added By</th>
                                            @endif
                                            <th>Main Category Name</th>
                                            <th>Sub Category Name</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                        @if(isset($dataArr) && count($dataArr) > 0)
                                            @foreach($dataArr as $data_key => $data_value)

                                                <tr data-row-id="{{ $data_value->id }}" id="record_{{ $data_value->id }}">
                                                    <td>{{ ++$data_key }}</td>
                                                    @if($is_admin == 1)
                                                    <td>{{ $data_value->first_name . " " . $data_value->last_name . " (" . $data_value->phone . ")" }}</td>
                                                    @endif
                                                    <td>{{ $data_value->category_name }}</td>
                                                    <td>{{ $data_value->subcategory_name }}</td>
                                                    <td>{{ $data_value->created_at }}</td>
                                                    <td>                                                    
                                                        <a href="javascript:void(0);" onclick="deletePopup('subcategory/delete/', {{ $data_value->id }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 4 ] }],
                "aaSorting": [[3,'DESC']]
            });  
        });  
    </script>
@endpush