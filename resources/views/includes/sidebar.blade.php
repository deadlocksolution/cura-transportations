
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

       
        <li>
            <a href="{{ route('vehicle') }}">
                <i class="zmdi zmdi-car-taxi mr-20"></i>
                <span class="right-nav-text">VEHICLE TYPES</span>
            </a>
        </li>

        <li>
            <a href="{{ route('country') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">COUNTRY AND CITY</span>
            </a>
        </li>

        <li>
            <a href="{{ route('dealer') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">Dealers</span>
            </a>
        </li>

    
        <li>
            <a href="{{ route('brand',array('auto')) }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">Brands</span>
            </a>
        </li>

        <li>
            <a href="javascript:void(0);" style="background: rgba(255, 255, 255, 0.05);">
                <div class="pull-left"><i class="zmdi zmdi-accounts mr-20"></i>
                    <span class="right-nav-text">Notification</span>
                </div>
                <div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div>
                <div class="clearfix"></div>
            </a>
            <ul id="dropdown_dr_lv5">
                <li>
                    <a href="{{ route('push-all') }}" style="color: #fff;"> Push to all </a>
                    <a href="{{ route('push-location') }}" style="color: #fff;"> Push by distance </a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="{{ route('terms') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">Terms and Conditions</span>
            </a>
        </li>
       
        <li>
            <a href="{{ route('configuration') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">Configuration</span>
            </a>
        </li>

    </ul>
</div>
<!-- /Left Sidebar Menu