<?php
extract($_REQUEST);

$subject = $subject;
$from = $from;
$recipient = $recipient;
$mess = $mess;  

send_mail_with_headers($subject, $from, $recipient, $mess, TRUE);

function send_mail_with_headers($subject, $from, $recipient, $mess, $isHTML = false, $filearr = array(), $path = "", $replyto = '')
{
    //set the message content type
    $content_type = "text/plain";
    if ($isHTML == true) {
        $content_type = "text/html";
    }
    if (is_array($recipient)) {
        $to  = @$recipient[0];
        $cc  = @$recipient[1];
        $bcc = @$recipient[2];
    } else {
        $to  = $recipient;
        $cc  = "";
        $bcc = "";
    }
    //set the header
    $headers = "";
    if (count($filearr) > 0) {
// USE multipart mime message to send mail with attachment
        //unique mime boundry seperater
        $mime_boundary_value = md5(uniqid(time()));
        //set the headers
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary=\"$mime_boundary_value\";\r\n";
        $headers .= "If you are reading this, then upgrade your e-mail client to support MIME.\r\n";
        //set the message
        if ($mess != "") {
            $mess = "--$mime_boundary_value\n" .
                "Content-Type: $content_type; charset=\"iso-8859-1\"\n" .
                "Content-Transfer-Encoding: 7bit\n\n" .
                $mess . "\n\n";
        }
        for ($i = 0; $i < count($filearr); $i++) {
            // if the upload succeded, the file will exist
            if (file_exists($filearr[$i])) {
                $mess .= "--$mime_boundary_value\n";
                $mess .= "Content-Type: text/csv; name=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Disposition: attachment; filename=\"{$filearr[$i]}\"\n";
                $mess .= "Content-Transfer-Encoding: base64\n\n";
                //read file data
                $file = fopen($filearr[$i], 'rb');
                $data = fread($file, filesize($filearr[$i]));
                fclose($file);
                //encode file data
                $data = chunk_split(base64_encode($data));
                $mess .= $data . "\n\n";
            }
        }
        $mess .= "--$mime_boundary_value--\n"; //end message
    } else {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-Type: ' . $content_type . '; charset=iso-8859-1' . "\r\n";
    }
    // To send HTML mail, the Content-type header must be set

    $headers .= 'From: ' . $from . "\r\n";
    //$bcc != '' ? $headers.="Bcc: $bcc \r\n" : '';
    $cc != '' ? $headers .= "cc:   $cc \r\n" : '';

    $response = mail($to, $subject, $mess, $headers); //this function will send mail
}
