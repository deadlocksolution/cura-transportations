<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSetting extends Model
{
    public $timestamps    = false;
    protected $primaryKey = 'NotificationSettingId';
    protected $table      = 'tbl_notificationsettings';

}
