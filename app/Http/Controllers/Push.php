<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Notification;
use App\SentPushLog;
use App\User;
use App\Vehicle;
use App\Notificationsolog;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Validator;

class Push extends Controller
{

    public function Index()
    {
        return view('admin.Notification.push');
    }

    public function Location()
    {
        $dataArr = Notification::select()
            ->where('NotificationType', '2')
            ->orderBy("NotificationId", "DESC")
            ->get();

        return view('admin.Notification.pushByLocation', compact('dataArr'));
    }

    public function Store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'NotificationText' => 'required',
        ]
        );

        $notificationType = '1';
        $message          = $request->NotificationText;

        // $update = NotificationSetting::firstOrNew(['UserId' => $UserId]);

        $dataArr = User::where('UserType', '1')
            ->Join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_users.UserId')
            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'tbl_users.UserId')
            ->where('ns.AllPush', 1)
            ->get();

        $resArr = array();
        if (count($dataArr) > 0) {

            $notification                   = new Notification();
            $notification->NotificationText = $message;
            $notification->NotificationType = $notificationType;
            $notification->save();

            foreach ($dataArr as $ke => $va) {

                $sendPush = $this->push_notification($va['DeviceToken'], $message, $notificationType, $va['DeviceType']);

                $sentPushLog                 = new SentPushLog;
                $sentPushLog->UserId         = $va['UserId'];
                $sentPushLog->NotificationId = $notification->NotificationId;
                $sentPushLog->Message        = $message;
                $sentPushLog->CreatedAt      = date('Y-m-d H:i:s');
                $sentPushLog->UpdatedAt      = date('Y-m-d H:i:s');
                $sentPushLog->save();
            }

        }

        flash()->success('Send Push Successfully');
        return Redirect::to('push-all');
    }

    public function Create()
    {
        return view('admin.Notification.add');
    }

    public function Add(Request $request)
    {
        $notification                   = new Notification();
        $notification->Latitude         = $request->Latitude;
        $notification->Longitude        = $request->Longitude;
        $notification->Range            = $request->Range;
        $notification->NotificationText = $request->NotificationText;
        $notification->NotificationType = '2';
        $result                         = $notification->save();

        if ($result > 0) {
            flash()->success('Push By Location Inserted Successfully');
            return Redirect::to('push-location');
        } else {
            flash()->warning('Error on Create push by location');
            return Redirect::back();
        }

    }

    public function Edit($id)
    {
        $NotificationData = Notification::find($id);
        return view('admin.Notification.edit', compact('NotificationData'));
    }

    public function Update(Request $request)
    {

        $id                             = $request->id;
        $notification                   = Notification::find($id);
        $notification->NotificationText = $request->NotificationText;
        $notification->Latitude         = $request->Latitude;
        $notification->Longitude        = $request->Longitude;
        $notification->Range            = $request->Range;

        $result = $notification->save();

        flash()->success('Push By Location Updated Successfully');
        return Redirect::to('push-location');

    }

    public function Destroy($id)
    {

        Notification::where('NotificationId', $id)->delete();
        echo 'success';
        exit();
    }

    public function PostExpirationReminder()
    {

        $StartDateTime = date("Y-m-d H:i:s", strtotime('+24 hours'));
        $EndDateTime   = date("Y-m-d H:i:s", strtotime('+24 hours', strtotime($StartDateTime)));
        $data = Vehicle::select()
			            ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
			            ->join('tbl_users as u', 'u.UserId', '=', 'pu.UserId')
			            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'pu.UserId')
			            ->where('ns.PostExpirationReminder',1)
			            ->where('VehicleExpiryDate', '>=', (string) $StartDateTime)
			            ->where('VehicleExpiryDate', '<=', (string) $EndDateTime)
			            ->where('u.Status', '<>', '1')
			            ->get();
        foreach ($data as $value) {
        	echo "<pre>";
        	print_r($value);
        	// $dataArr = Notificationsolog::select()
						   //          ->where('NotificationType', '2')
						   //          ->orderBy("NotificationId", "DESC")
						   //          ->get();
            $Message = "Your post is going to be exprire soon please update.";
            $this->push_notification($value->DeviceToken, $Message, '5', $value->DeviceType);
        }
    }

    public function SendDealOftheWeekReminderToDealer()
    {

        if(date('D') == 'Thu') { 
	        $data = Vehicle::select()
				            ->join('tbl_users as u', 'u.UserId', '=', 'tbl_vehicles.UserId')
				            ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
				            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'u.UserId')
            				->where('ns.SendDealOftheWeekReminderToDealer',1)
				            ->where('IsOffer', '=', '1')
				            ->where('UserType', '=', '2')
				            ->groupBy('u.UserId')
				            ->get();
	        foreach ($data as $value) {
	            $Message = "Your deal of the week is going to be exprire soon. Please update.";
	            $this->push_notification($value->DeviceToken, $Message, '6', $value->DeviceType);
	        }
	    }
    }

    // public function SendDealOftheWeekReminderToDealer1()
    // {
    // 	DB::table('tbl_cronlog')->insert(['CreatedAt' => date('Y-m-d H:i:s'),
    // 									  'type' => 'five-m']);
    // }

    public function SendDealOftheWeekReminderToUser()
    {
    	//DB::table('tbl_cronlog')->insert(['CreatedAt' => date('Y-m-d H:i:s')]);
        if(date('D') == 'Sat') { 
	        $data = Vehicle::select()
				            ->join('tbl_users as u', 'u.UserId', '=', 'tbl_vehicles.UserId')
				            ->join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_vehicles.UserId')
				            ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'u.UserId')
            				->where('ns.SendDealOftheWeekReminderToUser',1)
				            ->where('Status', '=', '1')
				            ->groupBy('u.UserId')
				            ->get();

	        foreach ($data as $value) {
	            $Message = "Deal of the week is going to be exprire soon, have a look in offer section.";
	            $this->push_notification($value->DeviceToken, $Message, '7', $value->DeviceType);
	        }
	    }
    }

    public function Push(){

       // $data = $this->push_notification('f5ghMhvriXk:APA91bEz0TTMucMWF7gC-ObmJpZ15wiZ6pO182hOfJVd2wvTI_TpWepTAVkJTjERqcj468q5wUihTjsj0afl_yecVF5aG-0YjTmSqUO5QSdNFmjBZHc8IfFglDmgsPPucxv9IFbvDpjh','test-123-789','','Android');

        $data = $this->push_notification('coZvDwKs_T0:APA91bGS9L76v1Mi2VFmDRy44p7gGvF79NlWj5i5kMWMpL6GrLg96IYYeSKml_0Rk4xJGqzZiHSMj-tTU9ZHoGTlZFwgumJQX1b1VRQUTcuY5-_T0VDJi3M2d7kNcP8FyEkKr6ixdtvF','test-123-789','','Android');

       print_r($data);
    }

    public function push_notification($DeviceToken, $message, $type, $device_os)
    {
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $DeviceToken = array($DeviceToken);

        /*api_key available in:
        Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/

        // CURA
        $api_key = 'AAAAAdH7P6E:APA91bHC3IISlxHxcBOJDx-VluTkBwx7HMSF-u5nqBkY1tU5cG9RLRQe3497kJsjorsg_rgkjscRakClaTfnWTnWjv9aqG1kR9qpo1PMrYcbsxbQ2znMc8ngsBMybUTnjihaFGZYR24v';

        // Qpluz
        // $api_key = 'AAAAqgojNZ0:APA91bEYwAVIGCZX48Gdl8Oa5qUiwVA0kl-ayIM4gQ7IvhjUOElupMpZCMIf0q626qUyKanQUEtOXWrFJSar0cKQ9JvhOPivkuxR3nt3x_EB9gTkIVq-pnUcptMh2VcImgVPF5cstWS0';

        // Kaufen
        // $api_key = 'AAAATPTxRdc:APA91bHikdxIgF41UwA6GvQD3eeppYQZ_GThQQuls07Cu0uWYomrNiuLLF1LdRs3OYHx2F7SKrUEmuK0UyR7OjJpzHobQ-ESIb__6ugr26v13ae2f1MVGiWFnBMCR6zBDWwrdfdW7ipF';

        if ($device_os == "Ios" || $device_os == "ios") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                'priority'         => 10,
                'notification'     => array('title' => 'CURA', 'body' => $message, 'sound' => 'Default'),
                'data'             => array('message' => $message, 'type' => $type),
            );
        }

        if ($device_os == "Android" || $device_os == "android") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                // 'data'             => array("message" => $message, 'type' => $type),
                'data'             => array("message" => $message),
            );
        }

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $api_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        // $this->load->helper('file');
        $log = "---------------------------------------------------------------------------\n\n";
        $log .= "LogAt: " . gmdate('Y-m-d H:i:s') . "\n\n";
        $log .= "Type: " . $type . "\n\n";
        $log .= "Message: " . $message . "\n\n";
        $log .= "Device Os: " . $device_os . "\n\n";
        // $log .="Data: [Order->".$order_id."]--[restaurant_name->".$restaurant_name."]--[pickup_address->".$pickup_address."]--[delivery_address->".$delivery_address."]--[order_request_id->".$order_request_id."]--[customer_name->".$customer_name."]\n\n";
        // $log .="ids: ".json_encode($device_ids)."\n\n";
        $log .= "Result: " . $result . "\n\n";
        $log .= "---------------------------------------------------------------------------\n\n";
        $file_path = "LIVE_PUSH_DEBUG1.txt";
        if (file_exists($file_path)) {
            Storage::put($file_path, $log);
        } else {
            Storage::put($file_path, $log);
        }


        return $result." ".$log;
    }
}
