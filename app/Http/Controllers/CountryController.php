<?php
namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CountryController extends Controller
{

    public function Index()
    {

        $countryArr = Country::select()->get();
        $dataArr = City::with('CountryData')->where('IsActive',1)->get();

        // echo "<pre>";
        // print_r($dataArr);exit;

        return view('admin.Country.index', compact('dataArr','countryArr'));

    }

    public function Create()
    {
        $countryArr = Country::select()->get();
        return view('admin.Country.add',compact('countryArr'));
    }

    public function Store(Request $request)
    {

        $country                    = new Country();
        $country->CountryName       = $request->CountryName;
        $country->save();

        return view('admin.Country.index');

    }

    public function Edit($id)
    {
        $BrandData = Brand::find($id);
        return view('admin.Brand.edit', compact('BrandData'));
    }

    public function StoreCity(Request $request){

        $city                 = new City();
        $city->CityName       = $request->CityName;
        $city->CountryId      = $request->CountryId;
        $city->Latitude       = $request->Latitude;
        $city->Longitude      = $request->Longitude;
        $city->Currency       = $request->Currency;
        $city->save();

        return view('admin.Country.index');
    }

    public function GetCity($id)
    {
        $CityData = City::where('CountryId',$id)->get();
        return json_encode($CityData);

    }

    public function Update(Request $request)
    {

        $id                       = $request->id;
        $brand                    = Brand::find($id);
        $brand->Name              = $request->name;
        $brand->VehicleType       = $request->vehicletype;
        $brand->Status            = $request->status;
        $brand->VehicleCategories = implode(',', $request->vehiclecategories);

        $result = $brand->save();

        flash()->success('Brand Updated Successfully');
        if( $request->vehicletype == 1 || $request->vehicletype == '1'){
            return Redirect::to('brand/type/auto');    
        } else {
            return Redirect::to('brand/type/motorcycle');    
        }

    }

    public function ActivateCity(Request $request)
    {

        City::where('CountryId', $request->CountryId)
            ->where('CityId', $request->CityId)
            ->update([
                'IsActive' => 1
            ]);

        return view('admin.Country.index');
        
    }

    public function Destroy($id)
    {

        Brand::where('BrandId', $id)->delete();
        echo 'success';
        exit();
    }

}
