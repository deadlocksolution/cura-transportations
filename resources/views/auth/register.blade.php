<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>B2B Hardware</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content=""/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- vector map CSS -->
        <link href="{{ asset("public/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css") }}" rel="stylesheet" type="text/css"/>
        
        <!-- select2 CSS -->
        <link href="{{ asset("public/vendors/bower_components/select2/dist/css/select2.min.css") }}" rel="stylesheet" type="text/css"/>

        <!-- switchery CSS -->
        <link href="{{ asset("public/vendors/bower_components/switchery/dist/switchery.min.css") }}" rel="stylesheet" type="text/css"/>
        
        <!-- multi-select CSS -->
        <link href="{{ asset("public/vendors/bower_components/multiselect/css/multi-select.css") }}" rel="stylesheet" type="text/css"/>
        
        <!-- bootstrap-select CSS -->
        <link href="{{ asset("public/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css") }}" rel="stylesheet" type="text/css"/>

        <!-- Custom CSS -->
        <link href="{{ asset("public/dist/css/style.css") }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Preloader-->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!--/Preloader-->

        <div class="wrapper  pa-0">
            <header class="sp-header">
                <div class="sp-logo-wrap pull-left">
                    <a href="{{ url('/login') }}">
                        <img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
                        <span class="brand-text">B2B Hardware</span>
                    </a>
                </div>
                <div class="form-group mb-0 pull-right">
                    <span class="inline-block pr-10">Already have an account?</span>
                    <a class="inline-block btn btn-primary  btn-rounded btn-outline" href="{{ url('/login') }}">Sign In</a>
                </div>
                <div class="clearfix"></div>
            </header>

            <!-- Main Content -->
            <div class="page-wrapper pa-0 ma-0 auth-page">
                <div class="container-fluid">
                    <!-- Row -->
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float" style="width: 600px;">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="mb-30">
                                            <h3 class="text-center txt-dark mb-10">Sign up to to B2B Hardware</h3>
                                            <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                        </div>	
                                        <div class="form-wrap">
                                            
                                            @if (session()->has('flash_notification.message'))
                                            <div class="alert alert-{{ session('flash_notification.level') }}">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                                                {!! session('flash_notification.message') !!}
                                            </div>
                                            @endif
                                            
                                            <form method="POST" action="javascript:void(0);" onsubmit="return validateSignup('{{ url('postSignup') }}');">
                                                {!! csrf_field() !!}
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <div class="radio-list">
                                                            <div class="radio-inline pl-0 col-sm-3">
                                                                <span class="radio radio-info">
                                                                    <input type="radio" name="user_type" id="user_type1" value="customer" checked="">
                                                                    <label for="user_type1">Customer</label>
                                                                </span>
                                                            </div>
                                                            <div class="radio-inline pl-0 col-sm-3">
                                                                <span class="radio radio-info">
                                                                    <input type="radio" name="user_type" id="user_type2" value="manufacturer">
                                                                    <label for="user_type2">Manufacturer</label>
                                                                </span>
                                                            </div>
                                                            <div class="radio-inline pl-0 col-sm-3">
                                                                <span class="radio radio-info">
                                                                    <input type="radio" name="user_type" id="user_type3" value="wholesaler">
                                                                    <label for="user_type3">Wholesaler</label>
                                                                </span>
                                                            </div>
                                                            <div class="radio-inline pl-0 col-sm-2">
                                                                <span class="radio radio-info">
                                                                    <input type="radio" name="user_type" id="user_type4" value="retailer">
                                                                    <label for="user_type4">Retailer</label>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="control-label mb-10" for="first_name">First Name *</label>
                                                        <input type="text" class="form-control" required="" id="first_name" name="first_name" placeholder="Enter First Name" value="{{ old('first_name') }}">
                                                    </div>

                                                    <div class="form-group  col-sm-6">
                                                        <label class="control-label mb-10" for="last_name">Last Name *</label>
                                                        <input type="text" class="form-control" required="" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{ old('last_name') }}">
                                                    </div>
                                                </div>
                                                    
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="control-label mb-10" for="phone">Phone *</label>
                                                        <input type="text" class="form-control" required="" id="phone" name="phone" placeholder="Enter Phone" value="{{ old('phone') }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="control-label mb-10" for="email">Email</label>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ old('email') }}">
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <span class="color-red" id="phone_already_exist_error" style="display: none;">Phone is already use. please use unique one.</span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="pull-left control-label mb-10" for="password">Password *</label>
                                                        <input type="password" class="form-control" required="" id="password" name="password" placeholder="Enter Password">
                                                    </div>

                                                    <div class="form-group col-sm-6">
                                                        <label class="pull-left control-label mb-10" for="cpassword">Confirm Password *</label>
                                                        <input type="password" class="form-control" required="" id="cpassword" placeholder="Confirm Password">
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <span class="color-red" id="password_mismatch_error" style="display: none;">Your password and confirmation password do not match.</span>
                                                        <span class="color-red" id="password_length_error" style="display: none;">Passwords must be at least 5 characters in length.</span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <label class="pull-left control-label mb-10" for="address">Address *</label>
                                                        <textarea class="form-control" required="" id="address" name="address" placeholder="Enter Address">{{ old('address') }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="pull-left control-label mb-10" for="country">Country *</label>
                                                        <select class="form-control select2" required="" name="country" id="country" onchange="changeCountry(this.value);">
                                                            <option value="">Select Country</option>
                                                            @foreach($country_data AS $country_value)
                                                            <option value="{{ $country_value['id'] }}">{{ $country_value['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="pull-left control-label mb-10" for="state">State *</label>
                                                        <select class="form-control select2" required="" name="state" id="state" onchange="changeState(this.value);">
                                                            <option value="">Select State</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="pull-left control-label mb-10" for="city">City *</label>
                                                        <select class="form-control select2" name="city" required="" id="city">
                                                            <option value="">Select City</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label class="control-label mb-10" for="pincode">Pincode</label>
                                                        <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter Pincode" value="{{ old('pincode') }}">
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label class="control-label mb-10" for="company_name">Company Name</label>
                                                        <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Company Name" value="{{ old('company_name') }}">
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <label class="control-label mb-10" for="category">Category</label>
                                                        <select class="form-control select2 select2-multiple" multiple="multiple" data-placeholder="Choose" name="category[]" id="category">
                                                            <option value="">Select Category</option>
                                                            @foreach($category_data AS $category_value)
                                                                <option value="{{ $category_value['id'] }}">{{ $category_value['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <div class="checkbox checkbox-primary pr-10 pull-left">
                                                            <input id="terms_checkbox" required="" type="checkbox">
                                                            <label for="terms_checkbox"> I agree to all Terms and Conditions</label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group text-center">
                                                    <button type="submit" class="btn btn-primary  btn-rounded">sign up</button>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->	
                </div>
            </div>
            <!-- /Main Content -->
        </div>
        <!-- /#wrapper -->

        <!-- JavaScript -->

        <!-- jQuery -->
        <script src="{{ asset("public/vendors/bower_components/jquery/dist/jquery.min.js") }}"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>
        <script src="{{ asset("public/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js") }}"></script>

        <!-- Slimscroll JavaScript -->
        <script src="{{ asset("public/dist/js/jquery.slimscroll.js") }}"></script>

        <!-- Switchery JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/switchery/dist/switchery.min.js") }}"></script>
        
        <!-- Select2 JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
        
        <!-- Multiselect JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/multiselect/js/jquery.multi-select.js") }}"></script>
        
        <!-- Bootstrap Select JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js") }}"></script>

        <!-- Custom JavaScript -->
        <script src="{{ asset("public/dist/js/custom.js") }}"></script>
        
        <!-- Init JavaScript -->
        <script src="{{ asset("public/dist/js/init.js") }}"></script>
                
        
        <div id="signup_success_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Verifying Account</h5>
                    </div>
                    <div class="modal-body">
                        <form action="javascript:void(0);">
                            <input type="hidden" id="signup_modal_user_id">
                            <div class="form-group">
                                <label for="signup_otp" class="control-label mb-10">OTP</label>
                                <input type="text" class="form-control only_numbers" id="otp_verify_popup" placeholder="Enter OTP Here">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" id="regenerate_otp_btn" onclick="regenerateUserOtp();">Regenerate OTP</button>
                        <button type="button" class="btn btn-primary" id="verified_btn" onclick="validateUserOtp();">Verify</button>
                        
                        <div class="clearfix mb-10"></div>
                        
                        <span class="color-red pull-left" id="regenerate_otp_success" style="display: none;">OTP Regenerated Successfully.</span>
                        <span class="color-red pull-left" id="user_not_found_error" style="display: none;">No User Found. Please Register again.</span>
                        <span class="color-red pull-left" id="user_already_verified_error" style="display: none;">User already verified.</span>
                        <span class="color-red pull-left" id="otp_invalid_error" style="display: none;">Invalid OTP. Please Try again.</span>
                        <span id="verify_success" class="color-green pull-left" style="display: none;">OTP Verified Successfully.</span>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
</html>