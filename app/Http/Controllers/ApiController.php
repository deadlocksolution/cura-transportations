<?php
namespace App\Http\Controllers;

use App\Api;
use App\ApiSession;
use App\Brand;
use App\Favourite;
use App\Http\Controllers\Controller;
use App\Line;
use App\Message;
use App\PushUser;
use App\User;
use App\Vehicle;
use App\Notification;
use App\SentPushLog;
use App\Config;
use App\Admin;
use Curl;
use Illuminate\Http\Request;
// use PHPMailer\PHPMailer\PHPMailer;
use PushNotification;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{

    public $timestamps = false;

    public function ApiRequest(Request $request)
    {

        $req    = $request->all();
        $file   = $request->file();
        $header = $request->header();

        $api_request = $req['api_request'];
        if (isset($req['data']) && !is_array($req['data'])) {
            $data = json_decode($req['data'], true);
        // }elseif ($req['data'] == '') {
        }else if (isset($req['data']) == '') {
            $data = array();
        } else {
            $data = $req['data'];
        }

        $request_api['data']   = $data;
        $request_api['file']   = $file;
        $request_api['header'] = $header;

        $Authorization = $request->header('authorization');
        $DeviceType    = $request->header('devicetype');
        $DeviceId      = $request->header('deviceid');
        $TestMode      = $request->header('TestMode');
        $TestMode      = (isset($TestMode) && $TestMode != "") ? $TestMode : "0";

        if ($api_request != "Login" && $api_request != "GuestLogin" && $api_request != "SocialLogin" && $api_request != "SignUp" && $api_request != "signUp_test" && $api_request != "ForgotPassword" && $api_request != "ChangePassword" && $api_request != "GenerateOtp"  && $api_request != "Logout" && $api_request != "forcefullyUpdateApp" && $api_request != "checkEmailAvailability"  && $TestMode != "1" && $api_request != "GetTermsCondition"){



            //--------------- CHECK AVALABILITY OF AUTHORIZATION & DEVICE TYPE & DEVICE ID ---------------//

            if ($Authorization != "" && $DeviceType != "" && $DeviceId != "") {

                //--------------- GET EXPLODE AUTHORIZATION ---------------//

                $access_header = explode(' ', $Authorization);

                //--------------- GET ACCESS KEY ---------------//

                if (!isset($access_header[0])) {
                    return $this->ErrorInvalid('access key');
                }
                $access_key = $access_header[0];

                //--------------- GET ACCESS TOKEN ---------------//

                if (!isset($access_header[1])) {
                    return $this->ErrorInvalid('access token');
                }

                //--------------- VALIDET ACCESS KEY ---------------//

                $AccessToken = $access_header[1];
                if ($access_key != "Elephant") {
                    return $this->ErrorDenied('Invalid access_key');
                }

                //--------------- CHECK API SESSION ---------------//

                $authentication = $this->AuthenticatApiCall($data['UserId'], $AccessToken, $DeviceType, $DeviceId);

                if ($authentication->count() == 0) {
                    return $this->ErrorDenied('User session expire.Please login again.');
                }

            } else {
                return $this->ErrorApi();
            }
        } else {

            $authentication = $this->AuthenticatApiId($req['api_id'], $req['api_secret']);
            if ($authentication->count() == 0) {
                return $this->ErrorDenied('App api id or api secret not valid.');
            }
        }
        // echo $api_request;

        return $this->$api_request($request_api);

    }

    public function ForcefullyUpdateApp($request)
    {
        $data = $request['data'];
        extract($data);
        if (!isset($data['AppVersion']) || $data['AppVersion'] == '') {
            return $this->ErrorInvalid('AppVersion');
        }
        if (!isset($data['DeviceType']) || $data['DeviceType'] == '') {
            return $this->ErrorInvalid('DeviceType');
        }
        extract($data);

        //--------------------- GET APP VERSION --------------------//

        $resVal = Api::select()->first();

        //--------------------- Check App Version --------------------//

        $AppUrl = '';
        $Status = '';
        if ($DeviceType == 'ios') {
            if ($AppVersion == $resVal[0]->IosAppVersion) {
                $Status = '0';
            } else {
                $Status = '1';
                $AppUrl = 'https://play.google.com/store/apps/';
            }

        } else if ($DeviceType == 'android') {
            if ($AppVersion == $resVal[0]->AndroidAppVersion) {
                $Status = '0';
            } else {
                $Status = '1';
                $AppUrl = '';
            }
        }

        //--------------------- API RESPONSE --------------------//

        $resArr           = array();
        $resArr['Status'] = (isset($Status) && $Status != "") ? (string) $Status : "";
        $resArr['AppUrl'] = (isset($AppUrl) && $AppUrl != "") ? $AppUrl : "";

        return $this->ApiSuccess($resArr, "A new version of App is available. Please update to version");
    }

    public function GenerateOtp($request)
    {
        $data    = $request['data'];
        $headers = $request['header'];

        extract($data);

        if (!isset($data['ContactNumber']) || $data['ContactNumber'] == '') {
            return $this->ErrorInvalid('ContactNumber');
        }
        if (!isset($data['CountryCode']) || $data['CountryCode'] == '') {
            return $this->ErrorInvalid('CountryCode');
        }


        $otp = mt_rand(100000, 999999);
        $id = "AC1c68a47435df243408e3c9e8695739ae";
        $token = "fb385fa2e4165a11ad56d81a9535d8ad";
        // $id = "AC7eef61196781e83be92185842ee084ea";
        // $token = "82e3c00eadf09cc2305c52deb156e52c";
        $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
        $from = "5092152847";
        // $from = "+447429008529";
        $to = $CountryCode.$ContactNumber; // twilio trial verified number
        $body = "Hello,Your OTP for login is ".$otp." Thank you!";
        $data = array (
            'From' => $from,
            'To' => $to,
            'Body' => $body,
        );
        $post = http_build_query($data);
        $x = curl_init($url );
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
        curl_close($x);
        // var_dump($post);
        // var_dump($y);
        
        $data = User::select()
            ->where('ContactNumber', $ContactNumber)
            ->where('CountryCode', $CountryCode)
            ->first();

        $resArr = array();

        if ($data) {

            //--------------- GENARE ACCESS TOKEN FOR API SESSION ---------------//

            $AccessToken   = $this->AccessToken();
            $AccessSession = date("Y-m-d H:i:s", strtotime("+365 day"));
            $AccessSession = strtotime($AccessSession);

            // $app->request->headers("Authorization")

            $DeviceType = $headers['devicetype'][0];
            $DeviceId   = $headers['deviceid'][0];

            if (isset($DeviceType) && isset($DeviceId)) {

                $get_api_session = ApiSession::select()
                    ->where('UserId', $data->UserId)
                    ->where('DeviceType', $DeviceType)
                    ->where('DeviceId', $DeviceId)
                    ->get();

                if ($get_api_session->count() > 0) {

                    $apisession = ApiSession::find($get_api_session[0]->SessionId)->delete();

                    $apisession                = new ApiSession();
                    $apisession->AccessToken   = $AccessToken;
                    $apisession->AccessSession = $AccessSession;
                    $apisession->DeviceType    = $DeviceType;
                    $apisession->DeviceId      = $DeviceId;
                    $apisession->UserId        = $data->UserId;
                    $apisession->CreatedAt     = date('Y-m-d H:i:s');
                    $apisession->UpdatedAt     = date('Y-m-d H:i:s');
                    $apisession->save();

                } else {

                    $apisession                = new ApiSession();
                    $apisession->AccessToken   = $AccessToken;
                    $apisession->AccessSession = $AccessSession;
                    $apisession->DeviceType    = $DeviceType;
                    $apisession->DeviceId      = $DeviceId;
                    $apisession->UserId        = $data->UserId;
                    $apisession->CreatedAt     = date('Y-m-d H:i:s');
                    $apisession->UpdatedAt     = date('Y-m-d H:i:s');
                    $apisession->save();
                }

            } else {

                $AccessToken = "";

            }
            
            $resArr['UserId'] = $data->UserId;
            $resArr['Otp']     = $otp;
            $resArr['IsNewUser']  = 0;
            $resArr['FirstName']     = (isset($data->FirstName) && $data->FirstName != "") ? $data->FirstName : "";
            $resArr['LastName']      = (isset($data->LastName) && $data->LastName != "") ? $data->LastName : "";
            $resArr['Email']         = (isset($data->Email) && $data->Email != "") ? $data->Email : "";
            $resArr['Type']          = (isset($data->UserType) && $data->UserType != "") ? $data->UserType : "";
            $resArr['ContactNumber'] = (isset($data->ContactNumber) && $data->ContactNumber != "") ? $data->ContactNumber : "";
            $resArr['ProfilePic']    = (isset($data->ProfilePic) && $data->ProfilePic != "") ? url($data->ProfilePic) : "";
            $resArr['AccessToken']   = (isset($AccessToken) && $AccessToken != "") ? $AccessToken : "";
            $resArr['CreatedAt']     = (isset($data->CreatedAt) && $data->CreatedAt != "") ? $data->CreatedAt : "";
            $resArr['UpdatedAt']     = (isset($data->UpdatedAt) && $data->UpdatedAt != "") ? $data->UpdatedAt : "";

            return $this->ApiSuccess($resArr, "User logging successfully.");
        } else {
            $resArr['Otp']     = $otp;
            $resArr['IsNewUser']  = 1;
            return $this->ApiSuccess($resArr, "Otp sent");
        }

    }

    public function SignUp($request)
    {

        $data    = $request['data'];
        $file    = $request['file'];
        $headers = $request['header'];
        extract($data);
        // 11 43 82 83 80

        if (!isset($data['FirstName']) || $data['FirstName'] == '') {
            return $this->ErrorInvalid('FirstName');
        }
        if (!isset($data['LastName']) || $data['LastName'] == '') {
            return $this->ErrorInvalid('LastName');
        }
        if (!isset($data['Email']) || $data['Email'] == '') {
            // return $this->ErrorInvalid('Email');
        }
        if (!isset($data['Latitude']) || $data['Latitude'] == '') {
            return $this->ErrorInvalid('Latitude');
        }
        if (!isset($data['Longitude']) || $data['Longitude'] == '') {
            return $this->ErrorInvalid('Longitude');
        }
        if (!isset($data['City']) || $data['City'] == '') {
            return $this->ErrorInvalid('City');
        }
        if (!isset($data['ContactNumber']) || $data['ContactNumber'] == '') {
            return $this->ErrorInvalid('ContactNumber');
        }
        if (!isset($data['ContactNumber']) || $data['ContactNumber'] == '') {
            return $this->ErrorInvalid('ContactNumber');
        }        

        $ProfilePic = '';
        if (isset($file) && !empty($file)) {
            $file            = (object) $file;
            $file            = $file->ProfilePic;
            $destinationPath = 'uploads/user_profile/';
            $file_name       = $this->FileName() . '.png';
            $file->move($destinationPath, $file_name);
            $profile_pic = 'uploads/user_profile/' . $file_name;
            $ProfilePic  = $profile_pic;
        }

        //--------------------- ADD USER DATA --------------------//

        $user                   = new User();
        $user->FirstName         = $FirstName;
        $user->LastName         = $LastName;
        $user->Email            = $Email;
        $user->CountryCode      = $CountryCode;
        $user->ContactNumber    = $ContactNumber;
        $user->ProfilePic       = $ProfilePic;
        $user->Latitude         = $Latitude;
        $user->Longitude        = $Longitude;
        $user->City             = $City;
        $user->save();
        $UserId = $user->UserId;

        //--------------------- API RESPONSE --------------------//

        $data = User::select()
            ->where('UserId', $UserId)
            ->first();

        //--------------- GENARE ACCESS TOKEN FOR API SESSION ---------------//

        $AccessToken   = $this->AccessToken();
        $AccessSession = date("Y-m-d H:i:s", strtotime("+365 day"));
        $AccessSession = strtotime($AccessSession);
        // $headers = apache_request_headers();

        $DeviceType = $headers['devicetype'][0];
        $DeviceId   = $headers['deviceid'][0];

        if (!empty($DeviceType) && !empty($DeviceId)) {

            $get_api_session = ApiSession::select()
                ->where('UserId', $UserId)
                ->where('DeviceType', $DeviceType)
                ->where('DeviceId', $DeviceId)
                ->get();

            if ($get_api_session->count() > 0) {

                $apisession = ApiSession::find('SessionId', $get_api_session->SessionId)->delete();

                $apisession                = new ApiSession();
                $apisession->AccessToken   = $AccessToken;
                $apisession->AccessSession = $AccessSession;
                $apisession->DeviceType    = $DeviceType;
                $apisession->DeviceId      = $DeviceId;
                $apisession->UserId        = $UserId;
                $apisession->CreatedAt     = date('Y-m-d H:i:s');
                $apisession->UpdatedAt     = date('Y-m-d H:i:s');
                $apisession->save();

            } else {

                $apisession                = new ApiSession();
                $apisession->AccessToken   = $AccessToken;
                $apisession->AccessSession = $AccessSession;
                $apisession->DeviceType    = $DeviceType;
                $apisession->DeviceId      = $DeviceId;
                $apisession->UserId        = $UserId;
                $apisession->CreatedAt     = date('Y-m-d H:i:s');
                $apisession->UpdatedAt     = date('Y-m-d H:i:s');
                $apisession->save();
            }

        } else {
            $AccessToken = "";

        }

        $resArr = array();
        if ($data->count() > 0) {
            $resArr['UserId']        = $data->UserId;
            $resArr['FirstName']     = (isset($data->FirstName) && $data->FirstName != "") ? $data->FirstName : "";
            $resArr['LastName']      = (isset($data->LastName) && $data->LastName != "") ? $data->LastName : "";
            $resArr['Email']         = (isset($data->Email) && $data->Email != "") ? $data->Email : "";
            $resArr['ContactNumber'] = (isset($data->ContactNumber) && $data->ContactNumber != "") ? $data->ContactNumber : "";
            $resArr['Latitude']      = (isset($data->Latitude) && $data->Latitude != "") ? $data->Latitude : "";
            $resArr['Longitude']     = (isset($data->Longitude) && $data->Longitude != "") ? $data->Longitude : "";
            $resArr['City']          = (isset($data->City) && $data->City != "") ? $data->City : "";
            $resArr['ProfilePic']    = (isset($data->ProfilePic) && $data->ProfilePic != "") ? url($data->ProfilePic) : "";
            $resArr['Status']        = (isset($data->Status) && $data->Status != "") ? $data->Status : "";
            $resArr['AccessToken']   = (isset($AccessToken) && $AccessToken != "") ? $AccessToken : "";
            $resArr['CreatedAt']     = (isset($data->CreatedAt) && $data->CreatedAt != "") ? $data->CreatedAt : "";
            $resArr['UpdatedAt']     = (isset($data->UpdatedAt) && $data->UpdatedAt != "") ? $data->UpdatedAt : "";
            
            return $this->ApiSuccess($resArr, "Successfully create account.");
        } else {
            return $this->ErrorDenied("Oops somthing went wrong,please try again after some time.");
        }

    }

    public function ChangeContactNumber($request)
    {
        $data    = $request['data'];
        $headers = $request['header'];

        extract($data);
        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['ContactNumber']) || $data['ContactNumber'] == '') {
            return $this->ErrorInvalid('ContactNumber');
        }
        if (!isset($data['CountryCode']) || $data['CountryCode'] == '') {
            return $this->ErrorInvalid('CountryCode');
        }
        if (!isset($data['Type']) || $data['Type'] == '') {
            return $this->ErrorInvalid('Type');
        }


        $user = $this->GetUserData($UserId);
        if (!$user) {
            return $this->ErrorDenied("User not available.");
        }

        $resArr = array();
        $otp ='';
        if($Type == 1){
            $otp = mt_rand(100000, 999999);
            $id = "AC1c68a47435df243408e3c9e8695739ae";
            $token = "fb385fa2e4165a11ad56d81a9535d8ad";
            $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
            $from = "5092152847";
            $to = $CountryCode.$ContactNumber; // twilio trial verified number
            $body = "Hello,Your OTP for change password is ".$otp." Thank you!";
            $data = array (
                'From' => $from,
                'To' => $to,
                'Body' => $body,
            );
            $post = http_build_query($data);
            $x = curl_init($url );
            curl_setopt($x, CURLOPT_POST, true);
            curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
            curl_setopt($x, CURLOPT_POSTFIELDS, $post);
            $y = curl_exec($x);
            curl_close($x);
            // var_dump($post);
            // var_dump($y);

        }else{

            $users               = User::find($UserId);
            $user->CountryCode   = $CountryCode;
            $user->ContactNumber = $ContactNumber;
            $user->save();
        }
        
        $data = User::select()
            ->where('ContactNumber', $ContactNumber)
            ->where('CountryCode', $CountryCode)
            ->first();

        $resArr['otp'] = $otp;
        return $this->ApiSuccess($resArr, 'Contact number has been sent successfully');
    }


    public function ForgotPassword($request)
    {
        $data = $request['data'];
        extract($data);

        if (!isset($data['Email']) || $data['Email'] == '') {
            return $this->ErrorInvalid('email_id');
        }
        extract($data);

        //--------------- CHECK EMAIL --------------------//

        $userData = User::select()
            ->where('Email', $Email)
            ->first();

        $url = "";

        if ($userData == 0) {
            return $this->ErrorDenied('User not registered.');
        } else if ($userData->count() > 0) {

            //--------------- SENT EMAIL FOR CHANGE PASSWORD --------------------//

            $RefKey = md5(rand(1000, 123456789456789));

            $user            = User::where('Email', $Email)->first();
            $user->RefKey    = $RefKey;
            $user->UpdatedAt = date('Y-m-d H:i:s');
            $user->save();

            $key     = $Email . "&" . $RefKey;
            $from    = "estuardo@aumenta.do";
            $subject = "El-Punto Forgot Password!";
            $url     = url('/') . "/changepassword/" . urlencode(base64_encode($key));

            $name = ($userData->FullName != '') ? $userData->FullName : $userData->UserName;

            $html = view('emails.user_forgot_password', ['name' => $name, 'url' => $url])->render();

            $this->SendMail($from, $Email, $html, $subject);
        }

        $resArr = array();
        return $this->ApiSuccess($resArr, 'A password reset link has been sent to your email.');

    }

    public function UpdateProfile($request)
    {

        $data = $request['data'];
        $file = $request['file'];
        $headers = $request['header'];
        extract($data);

        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['FirstName']) || $data['FirstName'] == '') {
            // return $this->ErrorInvalid('FirstName');
        }
        if (!isset($data['LastName']) || $data['LastName'] == '') {
            // return $this->ErrorInvalid('LastName');
        }
        if (!isset($data['Email']) || $data['Email'] == '') {
            // return $this->ErrorInvalid('Email');
        }
        if (!isset($data['DateOfBirth']) || $data['DateOfBirth'] == '') {
            // return $this->ErrorInvalid('DateOfBirth');
        }
        if (!isset($data['Gender']) || $data['Gender'] == '') {
            // return $this->ErrorInvalid('Gender');
        }
        if (!isset($data['Latitude']) || $data['Latitude'] == '') {
            return $this->ErrorInvalid('Latitude');
        }
        if (!isset($data['Longitude']) || $data['Longitude'] == '') {
            return $this->ErrorInvalid('Longitude');
        }
        if (!isset($data['City']) || $data['City'] == '') {
            return $this->ErrorInvalid('City');
        }

        $user = $this->GetUserData($UserId);
        if (!$user) {
            return $this->ErrorDenied("User not available.");
        }

        //--------------------- UPDATE USER DETAILS --------------------//

        $users = User::find($UserId);

        if (isset($file) && !empty($file)) {
            $file            = (object) $file;
            $file            = $file->ProfilePic;
            $destinationPath = 'uploads/user_profile/';
            $file_name       = $this->FileName() . '.png';
            $file->move($destinationPath, $file_name);
            $profile_pic       = 'uploads/user_profile/' . $file_name;
            $ProfilePic        = $profile_pic;
            $users->ProfilePic = $ProfilePic;
        }

        $user->Email         = $Email;
        $user->DateOfBirth   = $DateOfBirth;
        $user->Gender        = $Gender;
        $user->FirstName     = $FirstName;
        $user->LastName      = $LastName;
        $user->Latitude      = $Latitude;
        $user->Longitude     = $Longitude;
        $user->City          = $City;
        $user->save();

        //--------------------- API RESPONSE --------------------//

        $data = User::select()
            ->where('UserId', $UserId)
            ->first();
        // $AccessToken   = $this->AccessToken();
        $AccessToken   = explode(" ", $headers['authorization'][0])[1];

        $resArr = array();
        if ($data->count() > 0) {

            $resArr['UserId'] = $data->UserId;
            $resArr['FirstName']     = (isset($data->FirstName) && $data->FirstName != "") ? $data->FirstName : "";
            $resArr['LastName']      = (isset($data->LastName) && $data->LastName != "") ? $data->LastName : "";
            $resArr['Email']         = (isset($data->Email) && $data->Email != "") ? $data->Email : "";
            $resArr['Type']          = (isset($data->UserType) && $data->UserType != "") ? $data->UserType : "";
            $resArr['ContactNumber'] = (isset($data->ContactNumber) && $data->ContactNumber != "") ? $data->ContactNumber : "";
            $resArr['Latitude']      = (isset($data->Latitude) && $data->Latitude != "") ? $data->Latitude : "";
            $resArr['Longitude']     = (isset($data->Longitude) && $data->Longitude != "") ? $data->Longitude : "";
            $resArr['City']          = (isset($data->City) && $data->City != "") ? $data->City : "";
            $resArr['ProfilePic']    = (isset($data->ProfilePic) && $data->ProfilePic != "") ? url($data->ProfilePic) : "";
            $resArr['DateOfBirth']   = (isset($data->DateOfBirth) && $data->DateOfBirth != "") ? $data->DateOfBirth : "";
            $resArr['Gender']        = (isset($data->Gender) && $data->Gender != "") ? $data->Gender : "";
            $resArr['AccessToken']   = (isset($AccessToken) && $AccessToken != "") ? $AccessToken : "";
            $resArr['CreatedAt']     = (isset($data->CreatedAt) && $data->CreatedAt != "") ? $data->CreatedAt : "";
            $resArr['UpdatedAt']     = (isset($data->UpdatedAt) && $data->UpdatedAt != "") ? $data->UpdatedAt : "";
            
            return $this->ApiSuccess($resArr, "Profile update successfully.");
        } else {
            return $this->ErrorDenied("Oops somthing went wrong,please try again after some time.");
        }
    }

    public function ChangePassword($request)
    {
        $data = $request['data'];
        extract($data);

        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['CurrentPassword']) || $data['CurrentPassword'] == '') {
            return $this->ErrorInvalid('CurrentPassword');
        }
        if (!isset($data['NewPassword']) || $data['NewPassword'] == '') {
            return $this->ErrorInvalid('NewPassword');
        }

        $user = $this->GetUserData($UserId);
        if (!$user) {
            return $this->ErrorDenied("User not available.");
        }
        //--------------------- CHECK USER CURRENT PASSWORD --------------------//

        $check = User::select()
            ->where('UserId', $UserId)
            ->where('Password', $CurrentPassword)
            ->first();

        if ($check->count() == 0) {
            return $this->ErrorDenied("Current Password is wrong.");
        }

        $user           = User::find($UserId);
        $user->password = $NewPassword;
        $user->save();

        //--------------------- API RESPONSE --------------------//

        $resArr = array();
        return $this->ApiSuccess($resArr, "Success! Your password has been changed. Please login.");

    }

    public function RegisterForPush($request)
    {
        $data = $request['data'];
        extract($data);
        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['DeviceId']) || $data['DeviceId'] == '') {
            //return $this->ErrorInvalid('device_id');
        }
        if (!isset($data['DeviceType']) || $data['DeviceType'] == '') {
            return $this->ErrorInvalid('DeviceType');
        }
        if (!isset($data['DeviceToken']) || $data['DeviceToken'] == '') {
            return $this->ErrorInvalid('DeviceToken');
        }
        if (!isset($data['CertificateType']) || $data['CertificateType'] == '') {
            return $this->ErrorInvalid('CertificateType');
        }

        //--------------------- CHECK USER AVAILABLE --------------------//

        $user = $this->GetUserData($UserId);
        if (!$user) {
            return $this->ErrorDenied("User not available.");
        }
        $DeletePush = PushUser::where(['UserId' => $UserId, 'DeviceType' => $DeviceType, 'DeviceId' => $DeviceId])->delete();

        //--------------------- ADD USER DATA --------------------//

        $push                  = new PushUser();
        $push->UserId          = $UserId;
        $push->DeviceId        = $DeviceId;
        $push->DeviceToken     = $DeviceToken;
        $push->DeviceType      = $DeviceType;
        $push->CertificateType = $CertificateType;
        $push->CreatedAt       = date('Y-m-d H:i:s');
        $push->UpdatedAt       = date('Y-m-d H:i:s');
        $push -> save();

        $resArr = array();
        return $this->ApiSuccess($resArr, "You Register for push successfully. ");

    }

    public function UpdateLatLong($request)
    {
        $distance = '1';
        $push_days = ' -1 day';

        $data = $request['data'];
        extract($data);

        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['UserLatitude']) || $data['UserLatitude'] == '') {
            return $this->ErrorInvalid('UserLatitude');
        }
        if (!isset($data['UserLongitude']) || $data['UserLongitude'] == '') {
            return $this->ErrorInvalid('UserLongitude');
        }


        $user = $this->GetUserData($UserId);
        if (!$user) {
            return $this->ErrorDenied("User not available.");
        }

        if($UserId == '-1' || $UserId == -1){

        } else {
            $data = $this->GetUserData($UserId);
            if (!$data) {
                return $this->ErrorDenied("User not available.");
            }    
        }

        //--------------------- UPDATE USER DETAILS --------------------//

        $getsentPush = SentPushLog::select()
                        ->where('Status', '1')
                        ->where('UserId', $UserId)
                        ->where('CreatedAt', '>=' , date('Y-m-d', strtotime($push_days)))
                        ->get();

        $toRemoveData = array();

        foreach ($getsentPush as $key => $value) {
            $toRemoveData[] = $value['NotificationId'];
        }

        $userRange = Notification::select()
                    ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                               cos( radians( latitude ) )
                               * cos( radians( longitude ) - radians(?)
                               ) + sin( radians(?) ) *
                               sin( radians( latitude ) ) )
                             ) AS Distance', [$UserLatitude, $UserLongitude, $UserLatitude])
                    ->where('NotificationType', '2')
                    ->where('CreatedAt', '>=' , date('Y-m-d', strtotime($push_days)))
                    ->get();
           
        $i='0';
        if (count($userRange) > 0) {
            foreach ($userRange as $key => $value) {
                if ($value['Distance'] <= $value['Range']) {
                    if (!in_array($value['NotificationId'], $toRemoveData)) {

                        $message = $value['NotificationText'];
                        $notificationType = 2;

                        // $userPushDetail = PushUser::where(['UserId' => $UserId, 'Status' => '1'])->get();
                        // mysqli_query($this->conn,"SET SESSION sql_mode=''");
                        $userPushDetail = User::where('UserType', '1')
                                        ->Join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_users.UserId')
                                        ->Join('tbl_notificationsettings as ns', 'ns.UserId', '=', 'tbl_users.UserId')
                                        ->where('ns.GeoLocaction',1)
                                        ->where('pu.UserId', $UserId)
                                        ->where('pu.Status', 1)
                                        ->where('tbl_users.UserType', '!=' , 2)
                                        ->groupBy('tbl_users.UserId')
                                        ->get();

                        if (count($userPushDetail) > 0) {

                            // $addPush = $this->add_pushNotification('', $message, $notificationType);

                            foreach ($userPushDetail as $ke => $va) {
  
                                $sendPush = $this->push_notification($va['DeviceToken'], $message, $notificationType, $va['DeviceType']);

                                $addToPushLog = $this->add_sentPushLog($va['UserId'], $message, $value['NotificationId'], $notificationType,'');
                            }
                        }

                    }
                }
            }
        }

        $update             = User::find($UserId);
        $update->Latitude   = $UserLatitude;
        $update->Longitude  = $UserLongitude;
        $update->UpdatedAt  = date('Y-m-d H:i:s');
        $result             = $update->save();

        //--------------------- API RESPONSE --------------------//

        $resArr = array();
        return $this->ApiSuccess($resArr, "Successfully Update latitude longitude");

    }

    public function GetUserDetails($request)
    {
        $data = $request['data'];
        extract($data);
        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }

        $data = $this->GetUserData($UserId);
        if (!$data) {
            return $this->ErrorDenied("User not available.");
        }

        //--------------  API RESPONSE   ----------------- //
        $resArr = array();

        $resArr['UserId']   = $data->UserId;
        $resArr['UserName'] = (isset($data->UserName) && $data->UserName != "") ? $data->UserName : "";
        $resArr['FullName'] = (isset($data->FullName) && $data->FullName != "") ? $data->FullName : "";

        $resArr['Email']            = (isset($data->Email) && $data->Email != "") ? $data->Email : "";
        $resArr['ContactNumber']    = (isset($data->ContactNumber) && $data->ContactNumber != "") ? $data->ContactNumber : "";
        $resArr['DateOfBirth']      = (isset($data->DateOfBirth) && $data->DateOfBirth != "") ? $data->DateOfBirth : "";
        $resArr['Gender']           = (isset($data->Gender) && $data->Gender != "") ? $data->Gender : "";
        $resArr['ProfilePic']       = (isset($data->ProfilePic) && $data->ProfilePic != "") ? url($data->ProfilePic) : "";
        $resArr['SocialProfilePic'] = (isset($data->SocialProfilePic) && $data->SocialProfilePic != "") ? $data->SocialProfilePic : "";
        $resArr['FacebookSocialId'] = (isset($data->FacebookSocialId) && $data->FacebookSocialId != "") ? $data->FacebookSocialId : "";
        $resArr['GoogleSocialId']   = (isset($data->GoogleSocialId) && $data->GoogleSocialId != "") ? $data->GoogleSocialId : "";
        $resArr['Status']           = (isset($data->Status) && $data->Status != "") ? $data->Status : "";
        $resArr['CreatedAt']        = (isset($data->CreatedAt) && $data->CreatedAt != "") ? $data->CreatedAt : "";
        $resArr['UpdatedAt']        = (isset($data->UpdatedAt) && $data->UpdatedAt != "") ? $data->UpdatedAt : "";

        return $this->ApiSuccess($resArr, "User details retrived.");
    }

    public function GetVehicleCategory($request)
    {
        $data = $request['data'];
        extract($data);

        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['VehicleType']) || $data['VehicleType'] == '') {
            return $this->ErrorInvalid('VehicleType');
        }

        $BrandId = (isset($data['BrandId']) && $data['BrandId'] != '') ? $data['BrandId'] : '';
        $LineId  = (isset($data['LineId']) && $data['LineId'] != '') ? $data['LineId'] : '';

        if($UserId == '-1' || $UserId == -1){

        } else {
            $data = $this->GetUserData($UserId);
            if (!$data) {
                return $this->ErrorDenied("User not available.");
            }    
        }
        

        //--------------  GET Vehicle Category   ----------------- //
        if ($BrandId != '' && $LineId != '') {
            $data              = Line::where(['LineId' => $LineId])->first();
            $VehicleCategories = $data->VehicleCategoryId;
            $data              = VehicleCategory::where(['VehicleType' => $VehicleType])->whereRaw("FIND_IN_SET(VehicleCategoryId,'$VehicleCategories')")->get();
        } else {
            $data = VehicleCategory::where(['VehicleType' => $VehicleType])->get();
        }

        $resArr = array();

        if ($data->count() > 0) {
            $i = 0;
            foreach ($data as $key => $value) {
                $resArr[$i]['VehicleCategoryId'] = $value->VehicleCategoryId;
                $resArr[$i]['VehicleName']       = $value->VehicleName;
                $resArr[$i]['Image']             = url($value->Image);
                $resArr[$i]['HighlightImage']    = url($value->HighlightImage);
                $i++;
            }

        }

        return $this->ApiSuccess($resArr, "Vehicle Category retrived.");

    }


    
    public function GetVehicles($request)
    {

        $data = $request['data'];
        extract($data);

        if (!isset($data['UserId']) || $data['UserId'] == '') {
            return $this->ErrorInvalid('UserId');
        }
        if (!isset($data['VehicleType']) || $data['VehicleType'] == '') {
            return $this->ErrorInvalid('VehicleType');
        }
        if (!isset($data['VehicleState']) || $data['VehicleState'] == '') {
            // return $this->ErrorInvalid('VehicleState');
        }
        if (!isset($data['IsFilter']) || $data['IsFilter'] == '') {
            return $this->ErrorInvalid('IsFilter');
        }
        if (!isset($data['Action']) || $data['Action'] == '') {
            return $this->ErrorInvalid('Action');
        }

        $Limit  = (isset($Limit) && $Limit != '') ? $Limit : 20;
        $Offset = (isset($Offset) && $Offset != '') ? $Offset : 0;


        if($UserId == '-1' || $UserId == -1){
            $UserType = '-1';
        } else {
            $data = $this->GetUserData($UserId);
            if (!$data) {
                return $this->ErrorDenied("User not available.");
            } 
            $UserType =  $data['UserType'] ; 
        }


        $Filter = (isset($Filter) && !empty($Filter)) ? $Filter : array();


        // Get Vehicles

        $data = $this->FilterVehicles($UserId, $VehicleType, $VehicleState, $Limit, $Offset, $IsFilter, $Action, $Filter);

        // print_r(json_encode($data));
        // exit();
        $resArr = array();
        $finalArr = array();

        if (count($data) > 0) {
            $i = 0;
            foreach ($data as $key => $value) {

                if ($Action == 2 || $Action == '2') {
                    if ($value->favourite_data->count() == 0) {
                        continue;
                    }

                }

                $UserCarcheckdata = CarcheckRequest::where('UserId',$UserId)
                                        ->where('VehicleId',$value->VehicleId)
                                        ->first();

                // $Images = json_decode($value->VehicleImages, true);
                // array_walk_recursive($Images, function (&$value) {
                //     $value = url($value);
                // });
                $Images = array();
                if($value->VehicleImages){
                    $Images = json_decode($value->VehicleImages, true);
                    // echo $value->VehicleId." ";
                    array_walk_recursive($Images, function (&$value) {
                        $value = url($value);
                    });
                }
                // echo "working ";


                $userData = $this->GetUserData($value->UserId);
                $resArr[$i]['VehicleId']               = $value->VehicleId;
                $resArr[$i]['UserId']                  = $value->UserId;
                $resArr[$i]['Type']                    = $UserType;
                $resArr[$i]['Phone']                   = $userData['ContactNumber'];
                $resArr[$i]['VehicleCategoryId']       = $value->VehicleCategoryId;
                $resArr[$i]['VehicleType']             = ($value->VehicleType == 1) ? 'Car' : 'Motorcycle';
                $resArr[$i]['VehicleState']            = ($value->VehicleState == 1) ? 'New' : 'Used';
                $resArr[$i]['VehicleImages']           = $Images;
                $resArr[$i]['BrandId']                 = $value->BrandId;
                $resArr[$i]['BrandName']               = $value->brand_data['Name'];
                $resArr[$i]['LineId']                  = $value->LineId;
                $resArr[$i]['LineName']                = $value->line_data['Name'];
                $resArr[$i]['CurrencyId']              = $value->CurrencyId;
                // $resArr[$i]['CurrencySymbol']          = base64_decode($value->currency_data['CurrencySymbol']);
                // $resArr[$i]['Price']                   = $value->Price;
                $resArr[$i]['CurrencySymbol']          = ($value->Price != '' && $value->Price !=0) ? base64_decode($value->currency_data['CurrencySymbol']) : '';      
                $resArr[$i]['Price']                   = ($value->Price != '' && $value->Price !=0) ? $value->Price : '';
                $resArr[$i]['TotalVehicleCount']       = $data->count();
                $resArr[$i]['CarCheck']                = $value->CarCheck;
                $resArr[$i]['SafeCheck']               = $value->SafeCheck;
                $resArr[$i]['IsOffer']                 = $value->IsOffer;
                $resArr[$i]['Pdf']                     = ($value->Pdf != '') ? url($value->Pdf) : '';
                $resArr[$i]['IsFavouriteByMe']         = (!empty($value->favourite_data) && $value->favourite_data->count() > 0) ? '1' : '0';
                $resArr[$i]['Model']                   = ($value->Model != '' && $value->Model !=0) ? $value->Model : '';
                $resArr[$i]['VehicleSpecification']    = $this->VehicleSpecification($value->VehicleId, 4);
                $resArr[$i]['VehicleDetails']          = $this->VehicleDetails($value->VehicleId,$UserId, 4);
                $resArr[$i]['IsCarCheckByMe']          = (isset($UserCarcheckdata) && $UserCarcheckdata != '') ? '1' : 0;
                $resArr[$i]['VehicleExpiryDate']       = $value->VehicleExpiryDate;
                $resArr[$i]['IsBanner']                = '0';


                $date1 = date("Y-m-d", strtotime($value->VehicleExpiryDate));
                $date2 = date("Y-m-d");

                $dateDiff = $this->dateDifference($date1, $date2);
                $resArr[$i]['DaysRemains'] = $dateDiff;


                $i++;
            }



            $res = array();
            $GetBanner = Banner::inRandomOrder()->get();
            $j=0;
            if(count($GetBanner) > 0){
                foreach ($GetBanner as $key => $value) {
                    $res[$j]['BannerId'] = $GetBanner[0]['BannerId'];
                    $res[$j]['DealerId'] = $GetBanner[0]['DealerId'];
                    $res[$j]['BannerImage'] = url($GetBanner[0]['BannerImage']);
                    $res[$j]['IsBanner'] = '1';
                    $j++;
                }

            }

            
            if(!empty($res)){
                if(count($resArr)>5){
                    $k=0;
                    
                    for ($i=1; $i<=count($resArr); $i++) {

                        if($i % 6 == 0 && $k < count($res) ){
                            $finalArr[] = @$res[$k];
                            $finalArr[] = @$resArr[$i-1];
                            $k++;
                        }else{
                            $finalArr[] = @$resArr[$i-1];
                        }
                    }
                    
                }else{
                    // $finalArr = array_splice($resArr, count($resArr), 0, $res[0]);
                    // array_splice($resArr, 0, 1, $res[0]);
                    $finalArr = $resArr;
                }
            }else{
                $finalArr = $resArr;
            }

        }

        return $this->ApiSuccess($finalArr, "Vehicles retrived.");

    }

    // ========================= GET FILTER VEHICLE -=================================
    public function convert_number($TravelDistance){

    	return number_format($TravelDistance);
        // $num = preg_replace("/(\d+?)(?=(\d\d)+(\d)(?!\d))(\.\d+)?/i", "$1,", $TravelDistance);
        // return $num;

    }

     //-------------- CALCULATE DATE DIFFERANCE ----------------- //

    function dateDifference($start_date, $end_date)
    {
        // calulating the difference in timestamps 
        $diff = strtotime($start_date) - strtotime($end_date);
        return ceil(abs($diff / 86400));
    }

    public function GetTermsCondition($request)
    {

        $TermsAndCondition = Admin::pluck('TermsAndCondition');
        return $this->ApiSuccess($TermsAndCondition, "Terms And Condition retrived.");
    }

    //-------------- API AUTHENTICATION METHODS ----------------- //

    public function AuthenticatApiCall($UserId, $AccessToken, $DeviceType, $DeviceId)
    {

        $AccessSession = strtotime(date('Y-m-d H:i:s'));

        $data = ApiSession::select()
            ->where('UserId', $UserId)
            ->where('DeviceType', $DeviceType)
            ->where('DeviceId', $DeviceId)
            ->where('AccessToken', $AccessToken)
            ->where('AccessSession', '>=', $AccessSession)
            ->get();

        return $data;
    }

    //--------------- CHECK API ID & SECRET ---------------//

    public function AuthenticatApiId($ApiId, $ApiSecret)
    {
        return $data = Api::select()
            ->where(['ApiId' => $ApiId])
            ->where(['ApiSecret' => $ApiSecret])
            ->get();

    }

    //--------------- GENERAT SECRET ---------------//

    public function SecretGenerator($Email, $Password)
    {
        $Email    = strrev($Email);
        $Password = strrev($Password);
        return md5($Email . $Password);
    }

    //-------------- API AUTHENTICATION METHODS END ----------------- //

    //-------------- API RESPONSE METHODS ----------------- //

    public function GetUserData($UserId)
    {
        $data = User::select()
            ->where('UserId', $UserId)
            ->where('Status', 1)
            ->first();

        return $data;
    }

    public function GetConfig()
    {
        // $data = Config::select()->first();
        $data = Admin::select()->first();
        return $data;
    }

    public function ApiSuccess($data = null, $msg)
    {
        $output           = array();
        $output['flag']   = 'true';
        $output['result'] = 'success';
        $output['msg']    = $msg;
        if (isset($data)) {
            array_walk_recursive($data, function (&$value) {
                $value = strval($value);
            });
        }

        $output['data'] = $data;
        return json_encode($output);
    }

    public function getVehicleData($relatedData,$UserId){
        $i = 0;
        $resArr1 = array();
        foreach ($relatedData as $key => $value) {

            // $Images = json_decode($value->VehicleImages, true);
            // array_walk_recursive($Images, function (&$value) {
            //     $value = url($value);
            // });
            $Images = array();
            if($value->VehicleImages){
                $Images = json_decode($value->VehicleImages, true);
                array_walk_recursive($Images, function (&$value) {
                    $value = url($value);
                });
            }

            $UserCarcheckdata = CarcheckRequest::where('UserId',$UserId)
                                ->where('VehicleId',$value->VehicleId)
                                ->first();

            $resArr1[$i]['VehicleId']         = $value->VehicleId;
            $resArr1[$i]['UserId']            = $value->UserId;
            $resArr1[$i]['VehicleCategoryId'] = $value->VehicleCategoryId;
            $resArr1[$i]['VehicleType']       = ($value->VehicleType == 1) ? 'Car' : 'Motorcycle';
            $resArr1[$i]['VehicleState']      = ($value->VehicleState == 1) ? 'New' : 'Used';
            $resArr1[$i]['VehicleImages']     = $Images;
            $resArr1[$i]['BrandId']           = $value->BrandId;
            $resArr1[$i]['BrandName']         = $value->brand_data['Name'];
            $resArr1[$i]['LineId']            = $value->LineId;
            $resArr1[$i]['LineName']          = $value->line_data['Name'];
            $resArr1[$i]['CurrencyId']        = $value->CurrencyId;
            $resArr1[$i]['CurrencySymbol']    = ($value->Price != '' && $value->Price !=0) ? base64_decode($value->currency_data['CurrencySymbol']) : '';
            $resArr1[$i]['Price']             = ($value->Price != '' && $value->Price !=0) ? $value->Price : '';
            $resArr1[$i]['CarCheck']          = $value->CarCheck;
            $resArr1[$i]['SafeCheck']         = $value->SafeCheck;
            $resArr1[$i]['IsCarCheckByMe']    = (isset($UserCarcheckdata) && $UserCarcheckdata != '') ? '1' : 0;
            $resArr1[$i]['Pdf']               = ($value->Pdf != '') ? url($value->Pdf) : '';
            $resArr1[$i]['Model']             = ($value->Model != '' && $value->Model !=0) ? $value->Model : '';
            $resArr1[$i]['VehicleDetails'] = $this->VehicleDetails($value->VehicleId,$UserId, 4);

            $i++;
        }
        return $resArr1;
    }

    public function ErrorDenied($msg)
    {
        return $this->apiError('DENIED', $msg);
    }

    public function ErrorApi()
    {
        return $this->apiError('API_ERROR', 'Bad API user or secret.');
    }

    public function ErrorInvalid($fieldname)
    {
        return $this->apiError('INVALID_INPUT', 'Invalid or missing ' . $fieldname . '.');
    }

    public function ApiError($declaration, $msg)
    {
        $output = array(
            'flag'        => 'false',
            'result'      => 'error',
            'declaration' => $declaration,
            'msg'         => $msg,
        );
        return json_encode($output);
    }

    public function CurrentDatetime()
    {
        return date("Y-m-d H:i:s");
    }

    //--------------- GENERAT FILE NAME ---------------//

    public function FileName()
    {
        $str       = strtoupper(md5(uniqid(rand(), true)));
        $file_name = substr($str, 0, 8) . '-' .
        substr($str, 8, 4) . '-' .
        substr($str, 12, 4) . '-' .
        substr($str, 16, 4) . '-' .
        substr($str, 20);
        return $file_name;
    }

    //--------------- GENERAT ACCESS TOKEN ---------------//

    public function AccessToken($length = 50)
    {
        $characters       = '@0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //-------------- API RESPONSE METHODS END ----------------- //

    public function SendMail($from, $recipient, $mess, $subject)
    {

        $response = Curl::to('http://aumentaapps.com/CURA/email.php')

            ->withData(['subject' => $subject, 'from' => $from, 'recipient' => $recipient, 'mess' => $mess])

            ->post();

        // dd($response);

    }

    //-------------- API SEND PUSH NOTIFICATION ----------------- //
          
    function add_pushNotification($UserId,$message,$notificationType){
        $notification = new Notification();
        $notification->UserId = $UserId;
        $notification->NotificationText = $message;
        $notification->NotificationType = $notificationType;
        $notification->save();

        return $notification->NotificationId;
    }

    function add_sentPushLog($UserId,$message,$notificationId,$notificationType,$vehicleId){
        $sentPushLog                   = new SentPushLog;
        $sentPushLog->UserId           = $UserId;
        $sentPushLog->VehicleId        = (isset($vehicleId) && $vehicleId != "") ? $vehicleId : 0;
        $sentPushLog->NotificationId   = $notificationId;
        $sentPushLog->NotificationType = $notificationType;
        $sentPushLog->Message          = $message;
        $sentPushLog->CreatedAt        = date('Y-m-d H:i:s');
        $sentPushLog->UpdatedAt        = date('Y-m-d H:i:s');
        $sentPushLog->save();
    }

    function push_notification($DeviceToken,$message,$type,$device_os){
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $DeviceToken = array($DeviceToken);
    
        /*api_key available in:
        Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/   

        $api_key = 'AAAAAdH7P6E:APA91bHC3IISlxHxcBOJDx-VluTkBwx7HMSF-u5nqBkY1tU5cG9RLRQe3497kJsjorsg_rgkjscRakClaTfnWTnWjv9aqG1kR9qpo1PMrYcbsxbQ2znMc8ngsBMybUTnjihaFGZYR24v';

        
        if($device_os == "Ios" || $device_os == "ios"){
            //echo "Ios";
            $fields = array(
                'registration_ids' => $DeviceToken,
                'priority' => 10,
                'notification' => array('title' => 'CURA', 'body' =>  $message ,'sound'=>'Default'),
                // 'notification' => array('title' => 'CURA', 'body' =>  $message ,'sound'=>'Default','message' => $message, 'type' => $type)
            // );),
                'data' => array('message' => $message, 'type' => $type)
            );
        }
        
        if($device_os == "Android" || $device_os == "android"){
             //echo "Android";
            $fields = array (
                'registration_ids' => $DeviceToken,
                'data' => array ("message" => $message,'type' => $type,'title' => 'CURA',)
            );
        }
        
        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$api_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        // $this->load->helper('file');
        $log = "---------------------------------------------------------------------------\n\n";
        $log .="LogAt: ".gmdate('Y-m-d H:i:s')."\n\n";
        $log .="Type: ".$type."\n\n";
        $log .="Message: ".$message."\n\n";
        $log .="Device Os: ".$device_os."\n\n";
        // $log .="Data: [Order->".$order_id."]--[restaurant_name->".$restaurant_name."]--[pickup_address->".$pickup_address."]--[delivery_address->".$delivery_address."]--[order_request_id->".$order_request_id."]--[customer_name->".$customer_name."]\n\n";
        // $log .="ids: ".json_encode($device_ids)."\n\n";
        $log .="Result: ".$result."\n\n";
        $log .= "---------------------------------------------------------------------------\n\n";
        $file_path = "LIVE_PUSH_DEBUG.txt";
        if(file_exists($file_path))
        {
            Storage::put($file_path, $log);
        }
        else
        {
            Storage::put($file_path, $log );
        }
       
        return $result;
    }

}
