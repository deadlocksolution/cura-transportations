<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>CURA | @yield('title')</title>
        
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- select2 CSS -->
        <link href="{{ asset("public/vendors/bower_components/select2/dist/css/select2.min.css") }}" rel="stylesheet" type="text/css"/>

        <!-- multi-select CSS -->
        <link href="{{ asset("public/vendors/bower_components/multiselect/css/multi-select.css") }}" rel="stylesheet" type="text/css"/>
        
        <!-- Morris Charts CSS -->
        <link href="{{ asset("public/vendors/bower_components/morris.js/morris.css") }}" rel="stylesheet" type="text/css"/>

        <!-- Data table CSS -->
        <link href="{{ asset("public/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css") }}" rel="stylesheet" type="text/css"/>

        <link href="{{ asset("public/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css") }}" rel="stylesheet" type="text/css">

        <!-- bootstrap-select CSS -->
        <link href="{{ asset("public/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css") }}" rel="stylesheet" type="text/css"/>	

        <!-- Bootstrap Switches CSS -->
        <link href="{{ asset("public/vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css") }}" rel="stylesheet" type="text/css"/>

        
        
        <!-- switchery CSS -->
        <link href="{{ asset("public/vendors/bower_components/switchery/dist/switchery.min.css") }}" rel="stylesheet" type="text/css"/>

        <!-- Custom CSS -->
        <link href="{{ asset("public/dist/css/style.css?v=5") }}" rel="stylesheet" type="text/css">

        <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>

        <link href="{{ asset("public/dist/dropzone/dropzone.css?v=1") }}" rel="stylesheet" type="text/css">

    </head>
    @if(empty(Session::get('AdminId')) || empty(Session::get('Email')))
    @php $url = route('login');  @endphp
    <script type="text/javascript">
        window.location.href = "{{ $url }}";
    </script>
    @endif

    <style type="text/css">
        .select2-selection.select2-selection--multiple{
            height: auto !important;
        }
        .loader_div{
            position: absolute;
            /*background: #7108d633;*/
            background: #cccccc61;
            top: 45%;
            left: 45%;
            z-index: 99999999;
            border-radius: 10px;
            padding: 2%;
            display: none;
        }
        /*-----------------LOADER : START----------------------*/
        .loader {
            border: 8px solid #f3f3f3;
            border-radius: 50%;
            border-top: 8px solid #667add;
            width: 80px;
            height: 80px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
          0% { -webkit-transform: rotate(0deg); }
          100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }

        /*-----------------LOADER : END----------------------*/

    </style>
    <body>
        <!-- Preloader -->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!-- /Preloader -->
        <div class="wrapper  theme-3-active pimary-color-blue">
            
            @include('includes/topbar')
            
            @include('includes/sidebar')
            
            <!-- Main Content -->
            <div class="page-wrapper">
                @yield('main_container')

                <!-- Footer -->
                <footer class="footer container-fluid pl-30 pr-30">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>{{ date("Y") }} &copy; CURA</p>
                        </div>
                    </div>
                </footer>
                <!-- /Footer -->

            </div>
            <!-- /Main Content -->

        </div>
        <!-- /#wrapper -->
        
        
        <div class="modal fade" id="deleteModal" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirmation Delete</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure want to delete this record ?</p>
                    </div>
                    <div class="modal-footer"> 
                        <a href="javascript:void(0);" type="button" id="delete_confirm_btn" onclick="deleteRecord()" class="btn btn-danger">Yes</a>
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button> 
                    </div> 
                </div> 
            </div>
        </div>

        <div class="modal fade" id="uploadPdfModal" role="dialog">
            <div class="modal-dialog"> 
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">UPLOAD DOCUMENTS</h4>
                    </div>
                    <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                        <div class="modal-body">
                            <p> 
                                <input type="hidden" id="VehicleId" name="VehicleId" value="">
                                <img src="" id="img" style="display: none;" height="200px" width="200px">
                                <div class="fileupload btn btn-info btn-anim">
                                    <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload PDF</span>
                                    <input type="file" class="upload" required="" name="pdf" id="pdf" accept="application/pdf">
                                </div>
                                <div id="element"></div>
                            </p>
                        </div>
                        <div class="modal-footer"> 
                            <a href="javascript:void(0);" type="button" id="save_btn" onclick="saveRecord()" class="btn btn-danger" disabled>Save</a>
                            <button class="btn btn-default" data-dismiss="modal" onclick="cancelRecord()">Cancel</button> 
                        </div> 
                    </form>
                </div> 
            </div>
        </div>

        <div class="loader_div">
            <div class="loader"></div>
        </div>

        <!-- JavaScript -->
        
        <!-- jQuery -->
        <script src="{{ asset("public/vendors/bower_components/jquery/dist/jquery.min.js") }}"></script>

        <!-- Select2 JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/select2/dist/js/select2.full.min.js") }}"></script>

        <!-- Multiselect JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/multiselect/js/jquery.multi-select.js") }}"></script>
        
        <!-- Bootstrap Core JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js") }}"></script>

        <!-- Data table JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js") }}"></script>

        <!-- Slimscroll JavaScript -->
        <script src="{{ asset("public/dist/js/jquery.slimscroll.js") }}"></script>

        <!-- Progressbar Animation JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js") }}"></script>
        <script src="{{ asset("public/vendors/bower_components/jquery.counterup/jquery.counterup.min.js") }}"></script>

        <!-- Fancy Dropdown JS -->
        <script src="{{ asset("public/dist/js/dropdown-bootstrap-extended.js") }}"></script>

        <!-- Sparkline JavaScript -->
        <script src="{{ asset("public/vendors/jquery.sparkline/dist/jquery.sparkline.min.js") }}"></script>

        <!-- Owl JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js") }}"></script>

        <!-- Bootstrap Select JavaScript -->
        <script src="{{ asset("public/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js") }}"></script>

        <!-- Init JavaScript -->
        <script src="{{ asset("public/dist/js/init.js") }}"></script>
        
        <!-- Custom JavaScript -->
        <script src="{{ asset("public/dist/js/custom.js?v=7") }}?<?= time() ?>"></script>

        <script src="{{ asset("public/dist/dropzone/dropzone.js") }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        @stack('scripts')
    </body>

</html>
