<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificationsolog extends Model
{
    public $timestamps    = false;
    protected $primaryKey = 'NotificationsoLogId';
    protected $table      = 'tbl_notificationsolog';
}
